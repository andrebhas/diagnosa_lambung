<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php echo $this->config->item('nama_sistem') ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="<?= base_url();?>assets/pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url();?>assets/pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url();?>assets/pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url();?>assets/pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="Andre Bhaskoro Suprayogi" name="author" />
    <?php $this->load->view('layout/_css') ?>
    <style>
        .dataTables_wrapper {
            min-height: 500px
        }
        .dataTables_processing {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 100%;
            margin-left: -50%;
            margin-top: -25px;
            padding-top: 20px;
            text-align: center;
            font-size: 1.2em;
            color:grey;
        }
    </style>
  </head>
  <body class="fixed-header menu-behind">
    <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">

      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
        <!-- <img src="<?= base_url();?>assets/img/logo_white.png" alt="logo" class="brand" data-src="<?= base_url();?>assets/img/logo_white.png" data-src-retina="<?= base_url();?>assets/img/logo_white_2x.png" width="78" height="22">
        -->
        <?php echo $this->config->item('nama_sistem') ?>
      </div>
      <!-- END SIDEBAR MENU HEADER-->

      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">

            <?php  $this->load->view('layout/_menu'); ?>
        
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->

    </nav>
    <!-- END SIDEBAR -->
    
    <!-- START PAGE-CONTAINER -->
    <div class="page-container ">

      <?php $this->load->view('layout/_header'); ?>

      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid padding-25 sm-padding-10">
                        
            <?php $this->load->view($content); ?>
                
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
        
        <!-- START COPYRIGHT -->
        <div class="container-fluid container-fixed-lg footer">

          <?php $this->load->view('layout/_footer'); ?>

        </div>
        <!-- END COPYRIGHT -->

      </div>
      <!-- END PAGE CONTENT WRAPPER -->

    </div>
    <!-- END PAGE CONTAINER -->

        <?php $this->load->view('layout/_scripts'); ?>

  </body>
</html>