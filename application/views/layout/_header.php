<!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
          <!-- LEFT SIDE -->
          <div class="pull-left full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
                <span class="icon-set menu-hambuger"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
          <div class="pull-center hidden-md hidden-lg">
            <div class="header-inner">
              <div class="brand inline font-heading">
                <!-- <img src="<?= base_url();?>assets/img/logo.png" alt="logo" data-src="<?= base_url();?>assets/img/logo.png" data-src-retina="<?= base_url();?>assets/img/logo_2x.png" width="78" height="22">
                -->
                <span class="semi-bold"><?php echo $this->config->item('nama_sistem') ?></span>
              </div>
            </div>
          </div>
          
        </div>
        <!-- END MOBILE CONTROLS -->

        <div class=" pull-left sm-table hidden-xs hidden-sm">
          <div class="header-inner">
            <div class="brand inline font-heading">
              <!-- img src="<?= base_url();?>assets/img/logo.png" alt="logo" data-src="<?= base_url();?>assets/img/logo.png" data-src-retina="<?= base_url();?>assets/img/logo_2x.png" width="78" height="22">
              -->
              <span class="semi-bold"><?php echo $this->config->item('nama_sistem') ?></span>
            </div>
            
            <!-- START NOTIFICATION LIST -->
            
            <!-- END NOTIFICATIONS LIST -->

            <!-- FITUR SEARCH
            <a href="#" class="search-link" data-toggle="search"><i class="pg-search"></i>Type anywhere to <span class="bold">search</span></a> </div>
             -->
          </div>
        </div>

        <div class=" pull-right">
          <?php if($this->ion_auth->logged_in()){ ?>
          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
              <span class="semi-bold"><?php echo $user->nama ?></span>
            </div>
            <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="<?= base_url('images/users/'.$user->user_img);?>" alt="" data-src="<?= base_url('images/users/'.$user->user_img);?>" data-src-retina="<?= base_url('images/users/'.$user->user_img);?>" width="32" height="32">
            </span>
              </button>
              <ul class="dropdown-menu profile-dropdown" role="menu">
                <li><a href="<?= base_url('akun/update/'.$user->id); ?>"><i class="pg-settings_small"></i>Profil</a>
                </li>
                <li class="bg-master-lighter">
                  <a href="<?= base_url('auth/logout'); ?>" class="clearfix">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- END User Info-->
          <?php } ?>
        </div>
      </div>
      <!-- END HEADER -->