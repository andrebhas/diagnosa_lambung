<div class="copyright sm-text-center">
    <p class="small no-margin pull-left sm-pull-reset">
        <span class="hint-text">Copyright &copy; <?php echo date('Y') ?> </span>
        <span class="font-montserrat"><?php echo $this->config->item('nama_sistem') ?></span>.
        <span class="hint-text">All rights reserved. </span>
        <!--
        <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
        -->
    </p>
    <p class="small no-margin pull-right sm-pull-reset">
        <a href="http://facebook.com/andrebhas">Andre Bhaskoro Suprayogi</a> <span class="hint-text">®</span>
    </p>
    <div class="clearfix"></div>
</div>