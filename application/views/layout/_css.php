<link href="<?= base_url();?>assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url();?>assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url();?>assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url();?>assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?= base_url();?>assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?= base_url();?>assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?= base_url();?>assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?= base_url();?>assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url();?>assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url();?>assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?= base_url();?>assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?= base_url();?>assets/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="<?= base_url();?>assets/pages/css/pages.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url();?>assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url();?>assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />


<!--[if lte IE 9]>
<link href="<?= base_url();?>assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
<script src="<?= base_url();?>assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>