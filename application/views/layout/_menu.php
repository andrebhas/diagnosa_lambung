<li id="dashboard-li" class="m-t-30">
    <a href="<?= base_url('dashboard') ?>" class="detailed">
        <span class="title">Home</span>
    </a>
    <span id="dashboard-span" class="icon-thumbnail"><i class="pg-home"></i></span>
</li>

<?php if (!$this->ion_auth->logged_in()){ ?>
<li id="diagnosa-li">
    <a href="<?= base_url('kasus_baru/create') ?>" class="detailed">
        <span class="title">Diagnosa</span>
    </a>
    <span class="diagnosa-span icon-thumbnail"><i class="fa fa-stethoscope"></i></span>
</li>
<li>
    <a href="<?= base_url('auth/login') ?>" class="detailed">
        <span class="title">Login</span>
    </a>
    <span class="icon-thumbnail"><i class="fa fa-sign-in"></i></span>
</li>
<?php } ?>



<?php if ($this->ion_auth->logged_in()){ ?>

<?php if ($this->ion_auth->is_admin()): ?>
   
    <li id="kasus_baru-li">
        <a href="<?= base_url('kasus_baru') ?>" class="detailed">
            <span class="title">Data Diagnosa</span>
        </a>
        <span class="kasus_baru-span icon-thumbnail"><i class="fa fa-address-card-o"></i></span>
    </li>
    <li id="users-li">
        <a href="<?= base_url('akun') ?>" class="detailed">
            <span class="title">Kelola User</span>
        </a>
        <span  class="users-span icon-thumbnail"><i class="fa fa-users"></i></span>
    </li>
    <!--
    <li>
        <a href="javascript:;"><span class="title">Users</span>
        <span class="arrow"></span></a>
        <span class="groups-span users-span icon-thumbnail"><i class="fa fa-users"></i></span>
        <ul class="sub-menu">
            <li id="users-li" >
                <a href="<?= base_url('akun') ?>">Data User</a>
                <span class="users-span icon-thumbnail"><i class="fa fa-users"></i></span>
            </li>
            <li id="groups-li">
                <a href="<?= base_url('groups') ?>">Hak Akses</a>
                <span class="groups-span icon-thumbnail"><i class="fa fa-user-md"></i></span>
            </li>
        </ul>
    </li>
    -->
<?php else: ?>
    <li id="kasus_baru-li">
        <a href="<?= base_url('kasus_baru') ?>" class="detailed">
            <span class="title">Data Diagnosa</span>
        </a>
        <span class="kasus_baru-span icon-thumbnail"><i class="fa fa-address-card-o"></i></span>
    </li>
    <li id="basis_kasus-li">
        <a href="<?= base_url('basis_kasus') ?>" class="detailed">
            <span class="title">Basis Kasus</span>
        </a>
        <span  class="basis_kasus-span icon-thumbnail"><i class="fa fa-archive"></i></span>
    </li>
    <li id="penyakit-li">
        <a href="<?= base_url('penyakit') ?>" class="detailed">
            <span class="title">Data Penyakit</span>
        </a>
        <span class="penyakit-span icon-thumbnail"><i class="fa fa-heartbeat"></i></span>
    </li>
    <li id="gejala-li">
        <a href="<?= base_url('gejala') ?>" class="detailed">
            <span class="title">Data Gejala</span>
        </a>
        <span  class="gejala-span icon-thumbnail"><i class="fa fa-frown-o"></i></span>
    </li>
<?php endif ?>

<?php } ?>
