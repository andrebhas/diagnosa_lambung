
<script type="text/javascript">
(function($) {
    'use strict';
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    
    var initTableWithSearch = function() {
        var table = $('#table-gejala');
        var settings = {
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };
        table.dataTable(settings);
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }
    initTableWithSearch();

    var initAktiveLink = function(){
        var uri1 = '<?php echo $this->uri->segment(1) ?>';
        if(uri1=='gejala'){
            $('#gejala-li').addClass('active');
            $('.gejala-span').addClass('bg-success');
        }
    }
    initAktiveLink();

})(window.jQuery);
</script>