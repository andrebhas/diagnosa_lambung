
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5>Gejala</h5>
        </div>
    </div>
</div>

<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
                Form <?= $button ?> Gejala
            </div>
            <div class="panel-body">
                <div class="row">
                  <div class="col-sm-10">


                <form  class="form-horizontal" role="form" action="<?php echo $action; ?>" method="post">
				    <div class="form-group">
                        <label for="varchar" class="col-sm-3 control-label">Kode</label>
                        <div class="col-sm-9">    
                            <input type="text" class="form-control" name="kode" id="kode" value="<?php echo $kode; ?>" readonly="readonly"/>
                            <span class="text-danger"><?php echo form_error('kode') ?></span>
                        </div>
                    </div>
				    <div class="form-group">
                        <label for="gejala" class="col-sm-3 control-label">Gejala</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="3" name="gejala" id="gejala" placeholder="Gejala"><?php echo $gejala; ?></textarea>
                            <span class="text-danger"><?php echo form_error('gejala') ?></span>
                        </div>
                    </div>
				    <div class="form-group">
                        <label for="double" class="col-sm-3 control-label">Bobot</label>
                        <div class="col-sm-9">    
                            <select name="bobot" id="bobot" class="cs-select cs-skin-slide" data-init-plugin="cs-select">
                                <option value="">Pilih... </option>
                                <option value="0" <?php if($bobot == "0"){echo "selected";} ?>>0</option>
                                <option value="0.1" <?php if($bobot == "0.1"){echo "selected";} ?>>0,1</option>
                                <option value="0.2" <?php if($bobot == "0.2"){echo "selected";} ?>>0.2</option>
                                <option value="0.3" <?php if($bobot == "0.3"){echo "selected";} ?>>0.3</option>
                                <option value="0.4" <?php if($bobot == "0.4"){echo "selected";} ?>>0.4</option>
                                <option value="0.5" <?php if($bobot == "0.5"){echo "selected";} ?>>0.5</option>
                                <option value="0.6" <?php if($bobot == "0.6"){echo "selected";} ?>>0.6</option>
                                <option value="0.7" <?php if($bobot == "0.7"){echo "selected";} ?>>0.7</option>
                                <option value="0.8" <?php if($bobot == "0.8"){echo "selected";} ?>>0.8</option>
                                <option value="0.9" <?php if($bobot == "0.9"){echo "selected";} ?>>0.9</option>
                                <option value="1" <?php if($bobot == "1"){echo "selected";} ?>>1</option>
                            </select>
                            <span class="text-danger"><?php echo form_error('bobot') ?></span>
                        </div>
                    </div>
				    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 

                    <div class="row"><br>
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-9">
                          <button class="btn btn-success" type="submit">Submit</button>
                          <a href="<?php echo site_url('gejala') ?>" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
				</form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 