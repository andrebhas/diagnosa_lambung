<!-- START JUMBOTRON -->
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5><i class="fa fa-home"></i> Home</h5>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="row">
            <!-- START Login Background Pic Wrapper-->
            <div class="bg-pic">
                <!-- START Background Pic-->
                <img width="805px" height="454px" src="<?= base_url() ?>assets/img/maagh.jpg" data-src="<?= base_url() ?>assets/img/maagh.jpg" alt="" class="lazy">
                <!-- END Background Pic-->
                <!-- START Background Caption-->
                <div class="bg-caption pull-up sm-pull-up text-white p-l-20 m-b-20">
                <h1 class="semi-bold text-white">Case Based Reasoning Menggunakan Algoritma KNN untuk Diagnosa Penyakit Lambung</h1>
                <p class="semi-bold text-white">Oleh : Andre Bhaskoro Suprayogi (11241010101045)</p>
                </div>
                <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
                <h2 class="semi-bold text-white">Program Studi Sistem Informasi</h2>
                <h1 class="semi-bold text-white">Universitas Jember</h1>
                <h1 class="semi-bold text-white">2017</h1>
                </div>
                <!-- END Background Caption-->
            </div>
        </div>
    </div>
    <div class="col-md-4 col-lg-3 col-xlg-2 ">
        <div class="row">
            <div class="col-md-12 m-b-10">
            <!-- START WIDGET D3 widget_graphTileFlat-->
            <div class="widget-8 panel no-border bg-success no-margin widget-loader-bar">
                <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                    <div class="panel-heading top-left top-right">
                        <div class="panel-title text-black hint-text">
                        <span class="font-montserrat fs-11 all-caps">Data Diagnosa <i class="fa fa-chevron-right"></i>
                        </span>
                        </div>
                        <div class="panel-controls">
                        <ul>
                            <li>
                            <a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                            </li>
                        </ul>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row-xs-height ">
                    <div class="col-xs-height col-top relative">
                    <div class="row">
                        <div class="col-sm-6">
                        <div class="p-l-20">
                            <?php
                                $total_basis_kasus = $this->Basis_kasus_model->total_rows();
                                $total_kasus_baru = $this->Kasus_baru_model->total_rows();
                                $jumlah_kasus = $total_basis_kasus + $total_kasus_baru;
                            ?>
                            <h3 class="no-margin p-b-5 text-white"><i class="fa fa-stethoscope"></i> <?= $jumlah_kasus ?></h3>
                            <p class="small hint-text m-t-5">
                                <?= $total_basis_kasus ?> Data Basis Kasus <br>
                                <?= $total_kasus_baru ?> Data Diagnosa
                            </p>
                        </div>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                    <div class='widget-8-chart line-chart' data-line-color="black" data-points="true" data-point-color="success" data-stroke-width="2">
                        <svg></svg>
                    </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-bottom">
                    <div class="progress progress-small m-b-20">
                        <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                        <div class="progress-bar progress-bar-white" style="width:90%"></div>
                        <!-- END BOOTSTRAP PROGRESS -->
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- END WIDGET -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 m-b-10">
            <!-- START WIDGET widget_progressTileFlat-->
            <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
                <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                    <div class="panel-heading  top-left top-right">
                        <div class="panel-title text-black">
                        <span class="font-montserrat fs-11 all-caps">Jenis Penyakit Lambung <i class="fa fa-chevron-right"></i>
                        </span>
                        </div>
                        <div class="panel-controls">
                        <ul>
                            <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i
                                class="portlet-icon portlet-icon-refresh"></i></a>
                            </li>
                        </ul>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                    <div class="p-l-20 p-t-15">
                        <h3 class="no-margin p-b-5 text-white"><i class="fa fa-heartbeat"></i>
                            <?php 
                                $total_penyakit = $this->Penyakit_model->total_rows();
                                echo $total_penyakit;
                            ?>
                        </h3>
                        <span class="small hint-text"></span>
                    </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-bottom">
                    <div class="progress progress-small m-b-20">
                        <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                        <div class="progress-bar progress-bar-white" style="width:90%"></div>
                        <!-- END BOOTSTRAP PROGRESS -->
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- END WIDGET -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 m-b-10">
            <!-- START WIDGET D3 widget_graphTileFlat-->
            <div class="widget-8 panel no-border bg-success no-margin widget-loader-bar">
                <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                    <div class="panel-heading top-left top-right">
                        <div class="panel-title text-black hint-text">
                        <span class="font-montserrat fs-11 all-caps">Data Gejala <i class="fa fa-chevron-right"></i></span>
                        </div>
                        <div class="panel-controls">
                        <ul>
                            <li>
                            <a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i
                                    class="portlet-icon portlet-icon-refresh"></i></a>
                            </li>
                        </ul>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row-xs-height ">
                    <div class="col-xs-height col-top relative">
                    <div class="row">
                        <div class="col-sm-6">
                        <div class="p-l-20">
                            <h3 class="no-margin p-b-5 text-white"><i class="fa fa-frown-o"></i> 27</h3>
                            <p class="small hint-text m-t-5">
                            </p>
                        </div>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                    <div class='widget-8-chart line-chart' data-line-color="black" data-points="true" data-point-color="success" data-stroke-width="2">
                        <svg></svg>
                    </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-bottom">
                    <div class="progress progress-small m-b-20">
                        <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                        <div class="progress-bar progress-bar-white" style="width:90%"></div>
                        <!-- END BOOTSTRAP PROGRESS -->
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- END WIDGET -->
            </div>
        </div>
    </div>
</div>
