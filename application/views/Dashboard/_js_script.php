<script>
(function($) {
    'use strict';
    <?php if($this->session->userdata('message') <> ''){ ?>
        var notif  = function(e){
        var message = '<?php if($this->ion_auth->logged_in()){ echo $this->session->userdata("message"); }else{ echo "Data Anda Berhasil Tersimpan di Sistem Kami, <br> Terima Kasih sudah berkontribusi untuk pengembangan CBR Diagnosa Lambung ";} ?>';
        var type = 'success';
        var position = 'top-right' ;// Placement of the notification
        $('body').pgNotification({
            style: 'flip',
            title: 'John Doe',
            message: message,
            position: position,
            timeout: 0,
            type: type,
        }).show();
        //e.preventDefault();
    }
    notif();
    <?php } ?>
    var initAktiveLink = function(){
        var uri1 = '<?php echo $this->uri->segment(1) ?>';
        if(uri1=='dashboard'){
            $('#dashboard-li').addClass('active');
            $('#dashboard-span').addClass('bg-success');
        }
    }
    initAktiveLink();    

})(window.jQuery);
</script>