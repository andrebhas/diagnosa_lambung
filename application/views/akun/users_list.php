<!-- START JUMBOTRON -->
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5>User List</h5>
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->
<div class="row" style="margin-bottom: 10px">
    <div class="col-md-12 text-center">
        <div style="margin-top: 4px"  id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        </div>
    </div>
</div>
<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
            <?php echo anchor(site_url('akun/create'), '<i class="fa fa-plus-square"></i><span class="bold"> Create New</span>', 'class="btn btn-success btn-cons btn-xs"'); ?></div>
            <div class="pull-right">
                <div class="col-xs-12">
                <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped dataTable no-footer" id="table-users">
                <thead>
                    <tr>
                        <th width="50px">No</th>
						<th>User Image</th>
						<th>Name</th>
						<th>Email</th>
						<th>Username</th>
						<th>Phone</th>
						<th>Alamat</th>
						<th>Last Login</th>
                        <th>Level</th>
						<th>Action</th>
                    </tr>
                </thead>
				<tbody>
            <?php
                $start = 0;
                foreach ($users_data as $users)
                {
            ?>
                    <tr>
						<td><?php echo ++$start ?></td>
						<td><img width="35" height="35" src="<?= base_url('images/users/'. $users->user_img)?>"></td>
						<td><?php echo $users->nama ?></td>
						<td><?php echo $users->email ?></td>
						<td><?php echo $users->username ?></td>
						<td><?php echo $users->phone ?></td>
						<td><?php echo $users->alamat ?></td>
						<td><?php echo date('d/m/Y', $users->last_login); ?></td>
                        <td><?php $level = $this->ion_auth->get_users_groups($users->id)->result(); foreach ($level as $lvl) {
                            echo $lvl->name." , ";
                        }?></td>
						<td style="text-align:center" width="200px">
						<?php 
							//echo anchor(site_url('akun/read/'.$users->id),'Detail'); 
                            echo anchor(site_url('akun/update/'.$users->id),'<i class="fa fa-edit"></i> Edit', 'class="btn btn-xs btn-warning"');  
                            if($user->id != $users->id){
                                echo anchor(site_url('akun/delete/'.$users->id),'<i class="fa fa-trash"></i> Delete','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                            } 
						?>
						</td>
					</tr>
            <?php
                }
            ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
       