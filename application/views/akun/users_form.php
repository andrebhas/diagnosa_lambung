<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5>Users</h5>
        </div>
    </div>
</div>

<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
                Form <?= $button ?> User
            </div>
            <div class="panel-body">
                
            <?php if ($button == 'Update'): ?>
                <?php echo form_open($action); ?>
            <?php else: ?>
                <?php echo form_open_multipart($action);?>
            <?php endif ?>

                <div class="row">
                    
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="varchar">Nama <?php echo form_error('name') ?></label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                        </div>
                        <div class="form-group">
                            <label for="varchar">No Telepon <?php echo form_error('phone') ?></label>
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo $phone; ?>" />
                        </div>
                        <div class="form-group">
                            <label for="varchar">Alamat <?php echo form_error('alamat') ?></label>
                            <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="<?php echo $alamat; ?>" />
                        </div>
                         <?php if ($button == 'Update'): ?>

                            <a data-toggle="modal" href="#modal_foto" class="btn btn-warning" >
                                <img width="150" height="150" src="<?php echo base_url('images/users/'.$user_img) ?>">
                            </a> 

                        <?php else: ?>

                            <div class="form-group">
                                <label for="varchar">User Image <?php echo form_error('user_img') ?></label>
                                <input type="file" name="user_img" class="default user_img"/>
                                <img id="blah" src="#" alt="your image" />
                            </div>
                        <?php endif ?>

                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="varchar">User Level<?php echo form_error('group_id') ?></label>
                            <select  <?php if($id == $user->id){ echo 'disabled ';}?> class="form-control" name="group_id" id="group_id">
                                <option value=""> Pilih .... </option>
                                <?php foreach ($grup as $g): ?>
                                   <option <?php if ($groupss == $g->name) echo ' selected="selected" '; ?> value="<?= $g->id ?>"> <?= $g->name ?> </option> 
                                <?php endforeach ?>
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="varchar">Email <?php echo form_error('email') ?></label>
                            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
                        </div>
                        <div class="form-group">
                            <label for="varchar">Username <?php echo form_error('username') ?></label>
                            <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
                        </div> 
                        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                        <?php if ($button == 'Update'): ?>
                                <button type="submit" class="btn btn-success">Submit</button> 
                                <a data-toggle="modal" href="#modal_password" class="btn btn-warning" > Change Password </a> 
                                <a href="<?php echo site_url('akun') ?>" class="btn btn-default">Cancel</a>
                        <?php else: ?>
                                <div class="form-group">
                                    <label for="varchar">Password <?php echo form_error('password') ?></label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" />
                                </div>
                                <div class="form-group">
                                    <label for="varchar">Konfirmasi Password <?php echo form_error('password2') ?></label>
                                    <input type="password" class="form-control" name="password2" id="password2" placeholder="Konfirmasi Password" />
                                </div>
                                <button type="submit" class="btn btn-success">Submit</button> 
                                <a href="<?php echo site_url('akun') ?>" class="btn btn-default">Cancel</a>
                        <?php endif ?>                        
                            
                    </div>

                </div>

			</form>


            </div>
        </div>
    </div>
</div> 


                            <div class="modal fade" id="modal_foto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Ganti Foto</h4>
                                        </div>
                                        <div class="modal-body">

                                            <?php 
                                                $link = site_url('akun/ubah_foto/'.$id);
                                                echo form_open_multipart($link) ;
                                            ?>

                                            <div class="form-group">
                                                <label for="varchar">Foto <?php echo $this->session->flashdata('error_gambar'); ?></label>
                                                <input type="file" name="user_img" class="default user_img"/>
                                                <img id="blah" src="#" alt="your image" />
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-warning" type="submit"> Submit</button>
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>



                             <div class="modal fade" id="modal_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Change Passwords</h4>
                                        </div>
                                        <div class="modal-body">

                                            <?php 
                                                $link = site_url('akun/ubah_password/'.$id);
                                                echo form_open($link);
                                            ?>

                                             <div class="form-group">
                                                <label for="varchar">New Password</label>
                                                <input type="password" class="form-control" name="password" id="password"  />
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-warning" type="submit"> Submit</button>
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>