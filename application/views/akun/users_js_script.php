<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}

$(".user_img").change(function(){
    readURL(this);
});
    
(function($) {
    'use strict';
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    
    var initTableWithSearch = function() {
        var table = $('#table-users');
        var settings = {
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };
        table.dataTable(settings);
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }
    initTableWithSearch();

    var initAktiveLink = function(){
        var uri1 = '<?php echo $this->uri->segment(1) ?>';
        if(uri1){
            $('#users-li').addClass('active');
            $('.users-span').addClass('bg-success');
        }
    }
    initAktiveLink();

})(window.jQuery);
</script>