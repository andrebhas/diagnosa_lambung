<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5><i class="fa fa-stethoscope"></i> Detail Diagnosa <?= $kode ?></h5>
        </div>
    </div>
</div>
<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
                Hasil Diagnosa <?= $nama ?>
            </div>
            <div class="panel-body">

            <form class="form-horizontal" action="<?= base_url('kasus_baru/revise') ?>" method="post">

                <div class="row">

                    <div class="col-sm-7">
                        <table class="table">
                            <tr>
                                <td>Nama</td>
                                <td><?php echo $nama; ?></td>
                            </tr>
                            <tr>
                                <td>Usia</td>
                                <td><?php echo $usia; ?></td>
                            </tr>
                            <tr>
                                <td><p>Hasil Diagnosa</p> <p class="small">*Klik detail untuk melihat <br>alternatif pengobatan </p></td>                            
                                <td>
                                <?php 
                                    //echo $kode_similaritas;
                                    $kode_penyakit = explode(',',$kode_similaritas);
                                    // print_r($kode_penyakit);
                                    // $count_kode = count($kode_penyakit);
                                    foreach ($kode_penyakit as $key => $value) {
                                        if($value){
                                            $penyakit = $this->Basis_kasus_model->get_by_kode($value);
                                            //echo '<input type="text" name="kode_penyakit[]" value="'.$penyakit->kode.'" id="'.$penyakit->kode.'">';
                                            echo '<p>'.$penyakit->kode.' <a href="#" data-toggle="modal" data-target="#modal_detail" data-whatever="'.$penyakit->kode.'" data-penyakit="'.$penyakit->penyakit.'" data-penyebab="'.$penyakit->penyebab.'" data-pengobatan="'.$penyakit->pengobatan.'">'.$penyakit->penyakit.'</a> </p>';
                                        }
                                    }
                                    // foreach ($hasil_diagnosa as $key => $value) {
                                    //     $penyakit = $this->Basis_kasus_model->get_by_kode($key);
                                    //     echo '<p>';
                                    //     echo "<span class='text-danger'>".round($value,2)."% ";
                                    //     echo $penyakit->penyakit.'</span> | <a href="#" data-toggle="modal" data-target="#modal_detail" data-whatever="'.$penyakit->kode.'" data-penyakit="'.$penyakit->penyakit.'" data-penyebab="'.$penyakit->penyebab.'" data-pengobatan="'.$penyakit->pengobatan.'">detail</a> </p>';
                                    // }
                                ?>
                                </td> 
                                <!--                               
                                <input type="hidden" name="nilai_knn" id="nilai_knn" value="<?php echo max($hasil_diagnosa); ?>" />       
                                <input type="hidden" name="kode_similaritas" id="kode_similaritas" value="<?php //foreach($hasil_diagnosa as $key=>$value){echo $key.',';} ?>" />
                                -->
							</tr>
                            <!--
                            <?php 
                            if (!$this->ion_auth->is_admin()){
                            ?>
							<tr><td>Status</td><td>
                            <?php 
                                if($status==0){
                                    echo 'tidak tersimpan di basis kasus | <a href="'.base_url('kasus_baru/revise/'.$id).'" class="btn btn-success" type="submit">Revise</a>';
                                } else {
                                    echo 'tersimpan di basis kasus';
                                }
                            ?>
                            </td></tr>
                            <?php } ?>
                            -->
							<tr><td>Created At</td><td><?php echo $created_at; ?></td></tr>
	    					<tr><td>Updated At</td><td><?php echo $updated_at; ?></td></tr>
                        </table>        
                    </div>
                    <div class="col-sm-5">
                        <table class="table">
                             <tr>
                                <td align="center">Gejala yang dipilih</td>
                             </tr>
                             <tr>
                                <td>
                                <?php
                                    foreach ($gejala as $gj) {
                                        echo '
                                        <div class="checkbox check-primary">
                                            <input checked="checked" disabled="disabled" type="checkbox" value="'.$gj->kode_gejala.'" id="'.$gj->kode_gejala.'">
                                            <label for="'.$gj->kode_gejala.'">'.$gj->kode_gejala.' - '.$gj->gejala.'</label>
                                        </div>
                                        ';
                                    }
                                ?>
                                </td>
                             </tr>
                        </table>
                    </div>
                </div>
                <div class="row"><br>
                    <div class="col-sm-10">
                    <a href="<?php echo site_url('kasus_baru') ?>" class="btn btn-default">Back</a>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div> 

<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="modal_detailLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal_detailLabel">Detail </h4>
      </div>
      <div class="modal-body">
        <h3>Alternatif Pengobatan</h3>
        <div id="obat"></div>
        <h3 id="title_penyebab"></h3>
        <div id="penyebab"></div>
        <h3>Gejala</h3>
        <div id="gejala"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>