<!-- START JUMBOTRON -->
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5><i class="fa fa-address-card-o"></i> Data Diagnosa Lambung</h5>
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->
<div class="row" style="margin-bottom: 10px">
    <div class="col-md-12 text-center">
        <div style="margin-top: 4px"  id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        </div>
    </div>
</div>
<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
        <?php //echo anchor(site_url('kasus_baru/create'), '<i class="fa fa-stethoscope"></i><span class="bold"> Diagnosa</span>', 'class="btn btn-success btn-cons btn-xs"'); ?>
		<?php //echo anchor(site_url('kasus_baru/excel'), 'Excel', 'class="btn btn-warning btn-xs"'); ?>
		<?php //echo anchor(site_url('kasus_baru/word'), 'Word', 'class="btn btn-warning btn-xs"'); ?></div>
            <div class="pull-right">
                <div class="col-xs-12">
                <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped dataTable no-footer" id="table-kasus_baru">
                <thead>
                    <tr>
                        <th width="80px">No</th>
					    <th>Kode</th>
					    <th>Nama</th>
					    <th>Usia</th>
					    <!--
					    <th>Latar Belakang</th>
					    -->
                        <th>Persentase</th>
                        <!--
					    <th>Status</th>
                        -->
                        <th>Hasil Diagnosa</th>
                        <?php
                            if (!$this->ion_auth->is_admin()){
                                echo '<th width="200px">Action</th>';
                            }
                        ?>
                    </tr>
                </thead>
				<tbody>
                <?php
                    $start = 0;
                    foreach ($kasus_baru_data as $kasus_baru)
                    {
                ?>
                    <tr>
						<td><?php echo ++$start ?></td>
						<td><?php echo $kasus_baru->kode ?></td>
						<td><?php echo $kasus_baru->nama ?></td>
						<td><?php echo $kasus_baru->usia ?></td>
						<!--
						<td><?php echo $kasus_baru->latar_belakang ?></td>
						-->
						<td><?php echo round($kasus_baru->nilai_knn,2)." %" ?></td>
                        <td>
                            <?php
                                $kode_penyakit = explode(',',$kasus_baru->kode_similaritas);
                                    foreach ($kode_penyakit as $key => $value) {
                                        if($value){
                                            $penyakit = $this->Basis_kasus_model->get_by_kode($value);
                                            echo '<p>'.$penyakit->penyakit.'</p>';
                                        }
                                    }
                                    echo anchor(site_url('kasus_baru/read/'.$kasus_baru->id),'<i class="fa fa-eye"></i> Detail', 'class="btn btn-xs btn-info"'); 
                            ?>
                        </td>
                        <?php 
                            if (!$this->ion_auth->is_admin()){
                        ?>
						<td style="text-align:center" width="200px">
						<?php
                                if($kasus_baru->status == 0 AND $kasus_baru->nilai_knn < 80 ){
                                    echo anchor(site_url('kasus_baru/revise/'.$kasus_baru->id),'<i class="fa fa-edit"></i> Revise', 'class="btn btn-xs btn-success"');
                                } 
                                echo anchor(site_url('kasus_baru/delete/'.$kasus_baru->id),'<i class="fa fa-trash"></i> Delete','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                        ?>
                        </td>
                        <?php 
                            }							
						?>
					</tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
