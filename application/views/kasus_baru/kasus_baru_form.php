
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5><i class="fa fa-stethoscope"></i> Diagnosa Penyakit Lambung</h5>
        </div>
    </div>
</div>

<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
                Form Diagnosa
            </div>
            <div class="panel-body">
            
            <form  class="form-horizontal" role="form" action="<?php echo $action; ?>" method="post">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="varchar" class="col-sm-2 control-label">Kode</label>
                            <div class="col-sm-5">    
                                <input type="text" class="form-control" name="kode" id="kode" placeholder="Kode" value="<?php echo $kode; ?>" readonly="readonly"/>
                                <span class="text-danger"><?php echo form_error('kode') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="varchar" class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-5">    
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
                                <span class="text-danger"><?php echo form_error('nama') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="int" class="col-sm-2 control-label">Usia</label>
                            <div class="col-sm-5">    
                                <input type="text" class="form-control" name="usia" id="usia" placeholder="Usia" value="<?php echo $usia; ?>" />
                                <span class="text-danger"><?php echo form_error('usia') ?></span>
                            </div>
                        </div>
                        <!--
                        <div class="form-group">
                            <label for="varchar" class="col-sm-2 control-label">Latar Belakang</label>
                            <div class="col-sm-5">    
                                <input type="text" class="form-control" name="latar_belakang" id="latar_belakang" placeholder="Latar Belakang" value="<?php echo $latar_belakang; ?>" />
                                <span class="text-danger"><?php echo form_error('latar_belakang') ?></span>
                            </div>
                        </div>
                        -->
                        <div class="form-group">
                            <label for="varchar" class="col-sm-2 control-label">Pilih Gejala</label>
                            <div class="col-sm-10">    
                                <span class="text-danger"><?php echo form_error('gejala[]') ?></span>
                                <?php 
                                foreach ($dt_gejala as $d) {
                                    if($button == "Update") 
                                    {
                                        $this->db->where('kode_kasus', $kode);
                                        $this->db->where('kode_gejala', $d->kode);                             
                                        echo '
                                        <div class="checkbox check-primary">
                                            <input ';
                                        if($this->db->get('kasus_detail')->row())
                                        {
                                            echo 'checked="checked"';
                                        } 
                                        echo ' type="checkbox" name="gejala[]" value="'.$d->kode.'" id="'.$d->kode.'">
                                            <label for="'.$d->kode.'">'.$d->gejala.'</label>
                                        </div>
                                        ';
                                    }
                                    else 
                                    {
                                        echo '
                                        <div class="checkbox check-primary">
                                            <input type="checkbox" name="gejala[]" value="'.$d->kode.'" id="'.$d->kode.'">
                                            <label for="'.$d->kode.'">'.$d->gejala.'</label>
                                        </div>
                                        ';
                                    }
                                }
                                ?>

                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 

                        <div class="row"><br>
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-10">
                            <button class="btn btn-success" type="submit">Diagnosa</button>
                            <?php
                                if ($this->ion_auth->logged_in())
                                {
                            ?>
                                <a href="<?php echo site_url('kasus_baru') ?>" class="btn btn-default">Cancel</a>
                            <?php
                                } else {
                            ?>
                                <a href="<?php echo site_url() ?>" class="btn btn-default">Cancel</a>
                            <?php
                                }
                            ?>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div> 