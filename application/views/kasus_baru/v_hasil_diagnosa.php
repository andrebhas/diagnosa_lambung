
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5><i class="fa fa-stethoscope"></i> Hasil Diagnosa Penyakit Lambung</h5>
        </div>
    </div>
</div>

<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
                Hasil Diagnosa <?= $nama ?>
            </div>
            <div class="panel-body">

            <form  class="form-horizontal" role="form" action="<?php echo $action; ?>" method="post">



                <div class="row">

                    <div class="col-sm-7">
                        <table class="table">
                            <tr>
                                <td colspan="2" align="center">Diagnosa <?php echo $kode; ?></td>
                                <input type="hidden" name="kode" id="kode" value="<?php echo $kode; ?>"/>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td><?php echo $nama; ?></td>
                                <input type="hidden" name="nama" id="nama" value="<?php echo $nama; ?>" />
                            </tr>
                            <tr>
                                <td>Usia</td>
                                <td><?php echo $usia; ?></td>
                                <input type="hidden" name="usia" id="usia" value="<?php echo $usia; ?>" />
                            </tr>
                            <!--
                            <tr>
                                <td>Latar Belakang</td>
                                <td><?php echo $latar_belakang; ?></td>
                                <input type="hidden" name="latar_belakang" id="latar_belakang" value="<?php echo $latar_belakang; ?>" />
                            </tr>
                            -->
                            <tr>
                                <td><p>Hasil Diagnosa</p> <p class="small">*Klik detail untuk melihat <br>alternatif pengobatan </p></td>                             
                                <td>
                                <?php 
                                    foreach ($hasil_diagnosa as $key => $value) {
                                        $penyakit = $this->Basis_kasus_model->get_by_kode($key);
                                        echo '<p>';
                                        echo "<span class='text-danger'>".round($value,2)."% ";
                                        echo $penyakit->penyakit.'</span> | <a href="#" data-toggle="modal" data-target="#modal_detail" data-whatever="'.$penyakit->kode.'" data-penyakit="'.$penyakit->penyakit.'" data-penyebab="'.$penyakit->penyebab.'" data-pengobatan="'.$penyakit->pengobatan.'">detail</a> </p>';
                                    }
                                ?>
                                </td>                                
                                <input type="hidden" name="nilai_knn" id="nilai_knn" value="<?php echo max($hasil_diagnosa); ?>" />       
                                <input type="hidden" name="kode_similaritas" id="kode_similaritas" value="<?php foreach($hasil_diagnosa as $key=>$value){echo $key.',';
                                } ?>" />
                            </tr>
                        </table>        
                    </div>
                    <div class="col-sm-5">
                        <table class="table">
                             <tr>
                                <td align="center">Gejala yang dipilih</td>
                             </tr>
                             <tr>
                                <td>
                                <?php
                                    foreach ($gejala as $gj) {
                                        $gjla = $this->Gejala_model->get_by_kode($gj);
                                        echo '
                                        <div class="checkbox check-primary">
                                            <input checked="checked" disabled="disabled" type="checkbox" value="'.$gjla->kode.'" id="'.$gjla->kode.'">
                                            <label for="'.$gjla->kode.'">'.$gjla->kode.' - '.$gjla->gejala.'</label>
                                            <input name="gejala[]" type="hidden" value="'.$gjla->kode.'"/>
                                        </div>
                                        ';
                                    }
                                ?>
                                </td>
                             </tr>
                        </table>
                    </div>
                </div>
                <div class="row"><br>
                    <div class="col-sm-10">
                    <button class="btn btn-success" type="submit">Simpan</button>
                    <a href="<?php echo site_url('kasus_baru/create') ?>" class="btn btn-default">Diagnosa Ulang</a>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div> 

<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="modal_detailLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal_detailLabel">Detail </h4>
      </div>
      <div class="modal-body">
        <h3>Alternatif Pengobatan</h3>
        <div id="obat"></div>
        <h3 id="title_penyebab"></h3>
        <div id="penyebab"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

