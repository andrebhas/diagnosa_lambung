<script src="<?= base_url('assets/js/ckeditor/ckeditor.js') ?>"></script>
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5><i class="fa fa-archive"></i> Revise <?= $kode_kasus ?></h5>
        </div>
    </div>
</div>

<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
                Form  Basis Kasus
            </div>
            <div class="panel-body">
                
                <form  role="form" action="<?php echo $action; ?>" method="post">
                <div class="row">
                    <div class="col-sm-5">
                        <input type="hidden" name="id_user" id="id_user"value="<?php echo $id_user; ?>" />
                        <div class="form-group">
                            <label for="varchar">Kode</label>
                            <input type="text" class="form-control" name="kode" id="kode" value="<?php echo $kode_kasus; ?>" readonly="readonly" />
                            <span class="text-danger"><?php echo form_error('kode') ?></span>
                        </div>
                        <div class="form-group">
                            <label for="varchar">Jenis Penyakit Lambung</label><br> 
                            <select name="kode_penyakit" id="kode_penyakit" class="cs-select cs-skin-slide" data-init-plugin="cs-select">
                                <option value="">Pilih... </option>
                            <?php
                                // $kode_similaritas = explode(',',$kode_penyakit);
                                //     foreach ($kode_similaritas as $key => $value) {
                                //         if($value){
                                //             $penyakit = $this->Basis_kasus_model->get_by_kode($value);
                                //             echo '<option value="'.$penyakit->kode_penyakit.'">'.$penyakit->penyakit.'</option>';
                                //         }
                                //     }
                                $data_penyakit = $this->Penyakit_model->get_all();
                                foreach ($data_penyakit as $data) {
                                    echo '<option value="'.$data->kode.'">'.$data->penyakit.'</option>';
                                }
                            ?>
                            </select>
                            <a data-toggle="modal" data-target="#myModal" class="btn btn-success btn-sm">Tambah Jenis Penyakit</a>
                            <span class="text-danger"><?php echo form_error('kode_penyakit') ?></span>
                        </div>
                        <div class="form-group">
                            <label for="penyebab">Penyebab</label>
                            <textarea class="form-control" rows="3" name="penyebab" id="penyebab" placeholder="Penyebab"></textarea>
                            <span class="text-danger"><?php echo form_error('penyebab') ?></span>
                        </div>
                        <div class="form-group">
                            <label for="pengobatan">Pengobatan</label>
                            <textarea class="form-control" rows="3" name="pengobatan" id="pengobatan" placeholder="Pengobatan"></textarea>
                            <span class="text-danger"><?php echo form_error('pengobatan') ?></span>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <textarea class="form-control" rows="3" name="keterangan" id="keterangan" placeholder="Keterangan"></textarea>
                            <span class="text-danger"><?php echo form_error('keterangan') ?></span>
                        </div>

                    </div>
                    <div class="col-sm-7">
                        <h3>Pilih Gejala</h3>
                        <span class="text-danger"><?php echo form_error('gejala[]') ?></span>
                        <?php 
                        foreach ($gejala as $d) {
                            echo '
                            <div class="checkbox check-primary">
                                <input disabled type="checkbox" value="'.$d->kode_gejala.'" id="'.$d->kode_gejala.'" checked="checked" >
                                <label for="'.$d->kode_gejala.'">'.$d->kode_gejala.' - '.$d->gejala.'</label>
                            </div>
                            ';
                        }
                        ?>
                    </div>
                </div>
    <?php
        foreach ($gejala as $d) {
                            echo '
                           <input type="hidden" name="gejala[]" value="'.$d->kode_gejala.'>
                            ';
                        }
    ?>
                <div class="row">
                    <div class="col-sm-3">
                        <p>I hereby certify that the information above is true and accurate. </p>
                    </div>
                    <div class="col-sm-9">
                        <button class="btn btn-success" type="submit">Submit</button>
                        <a href="<?php echo site_url('kasus_baru') ?>" class="btn btn-default">Cancel</a>
                    </div>
                </div>
                </form>

            </div>

        </div>
    </div>
</div> 

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Jenis Penyakit</h4>
      </div>
      <div class="modal-body">
        <?php
            $first_row = $this->Penyakit_model->get_first_row();
            if (!$first_row ) {
                $kode = "P1";
            } else {
                $kodenya = $first_row->kode;
                $pisah = explode('P',$kodenya);
                $num = $pisah[1] + 1;
                $kode = "P".$num;
            }
        ?>
        <form  class="form-horizontal" role="form" action="<?php echo base_url('kasus_baru/tambah_penyakit_action/'.$id_kasus_baru) ?>" method="post">
				    <div class="form-group">
                        <label for="varchar" class="col-sm-3 control-label">Kode</label>
                        <div class="col-sm-9">    
                            <input type="text" class="form-control" name="kode" id="kode" value="<?php echo $kode; ?>" readonly="readonly" />
                        </div>
                    </div>
				    <div class="form-group">
                        <label for="varchar" class="col-sm-3 control-label">Nama Penyakit</label>
                        <div class="col-sm-9">    
                            <input type="text" class="form-control" name="penyakit" id="penyakit" placeholder="Penyakit" value="" /
                            <span class="text-danger"><?php echo form_error('penyakit') ?></span>
                        </div>
                    </div>				
      </div>
      <div class="modal-footer">
        <button class="btn btn-success" type="submit">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
    CKEDITOR.replace( 'penyebab' );
    CKEDITOR.replace( 'pengobatan' );
    CKEDITOR.replace( 'keterangan' );
</script>