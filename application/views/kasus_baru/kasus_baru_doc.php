<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Kasus_baru List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Kode</th>
		<th>Nama</th>
		<th>Usia</th>
		<th>Latar Belakang</th>
		<th>Nilai Knn</th>
		<th>Status</th>
		<th>Created At</th>
		<th>Updated At</th>
		
            </tr><?php
            foreach ($kasus_baru_data as $kasus_baru)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $kasus_baru->kode ?></td>
		      <td><?php echo $kasus_baru->nama ?></td>
		      <td><?php echo $kasus_baru->usia ?></td>
		      <td><?php echo $kasus_baru->latar_belakang ?></td>
		      <td><?php echo $kasus_baru->nilai_knn ?></td>
		      <td><?php echo $kasus_baru->status ?></td>
		      <td><?php echo $kasus_baru->created_at ?></td>
		      <td><?php echo $kasus_baru->updated_at ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>