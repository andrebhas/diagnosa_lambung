
<script type="text/javascript">
$('#modal_detail').on('show.bs.modal', function (event) {
  $('#isi').remove();
  $('#isi_penyebab').remove();
  $('#gejala').remove();
  var button = $(event.relatedTarget) 
  var recipient = button.data('whatever') 
  var penyakit = button.data('penyakit')
  var pengobatan =  button.data('pengobatan')
  var penyebab = button.data('penyebab')
  var link = '<?php echo base_url("kasus_baru/json_get_gejala/")?>'+recipient;
  var modal = $(this)
  modal.find('.modal-title').text('Detail ' + penyakit)
  $('#obat').append("<div id='isi'>"+pengobatan+"</div>")
  $('#title_penyebab').text('Penyebab '+penyakit)
  $('#penyebab').append("<div id='isi_penyebab'>"+penyebab+"</div>")
  $('#gejala').append("<ul>");  
    $.getJSON( link, function( data ) {
        $.each( data, function( key, val ) {
           $('#gejala').append("<li>"+val.kode +" "+ val.gejala+" </li>"); 
        }); 
    });
  $('#gejala').append("</ul>"); 
});

(function($) {
    'use strict';
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    
    var initTableWithSearch = function() {
        var table = $('#table-kasus_baru');
        var settings = {
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };
        table.dataTable(settings);
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }
    initTableWithSearch();

    var initAktiveLink = function(){
        var uri1 = '<?php echo $this->uri->segment(1) ?>';
        var uri2 = '<?php echo $this->uri->segment(2) ?>';
        if(uri2 == 'create'){
            $('#diagnosa-li').addClass('active');
            $('.diagnosa-span').addClass('bg-success');
        } else {
            $('#kasus_baru-li').addClass('active');
            $('.kasus_baru-span').addClass('bg-success');
        }
    }
    initAktiveLink();

})(window.jQuery);
</script>