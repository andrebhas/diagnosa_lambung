
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5>Penyakit</h5>
        </div>
    </div>
</div>

<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
                Form <?= $button ?> Penyakit
            </div>
            <div class="panel-body">
                <div class="row">
                  <div class="col-sm-10">


                <form  class="form-horizontal" role="form" action="<?php echo $action; ?>" method="post">
				    <div class="form-group">
                        <label for="varchar" class="col-sm-3 control-label">Kode</label>
                        <div class="col-sm-9">    
                            <input type="text" class="form-control" name="kode" id="kode" value="<?php echo $kode; ?>" readonly="readonly" />
                            <span class="text-danger"><?php echo form_error('kode') ?></span>
                        </div>
                    </div>
				    <div class="form-group">
                        <label for="varchar" class="col-sm-3 control-label">Penyakit</label>
                        <div class="col-sm-9">    
                            <input type="text" class="form-control" name="penyakit" id="penyakit" placeholder="Penyakit" value="<?php echo $penyakit; ?>" />
                            <span class="text-danger"><?php echo form_error('penyakit') ?></span>
                        </div>
                    </div>
				    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 

                    <div class="row"><br>
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-9">
                          <button class="btn btn-success" type="submit">Submit</button>
                          <a href="<?php echo site_url('penyakit') ?>" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
				</form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 