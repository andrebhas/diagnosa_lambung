<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Login <?php echo $this->config->item('nama_sistem') ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="<?= base_url() ?>assets/pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>assets/pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>assets/pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>assets/pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<?= base_url() ?>assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url() ?>assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url() ?>assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url() ?>assets/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="<?= base_url() ?>assets/pages/css/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="<?= base_url() ?>assets/pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  <body class="fixed-header ">
    <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic">

        <!-- START Background Pic-->
        <img src="<?= base_url() ?>assets/img/maagh.jpg" data-src="<?= base_url() ?>assets/img/maagh.jpg" alt="" class="lazy">
        <!-- END Background Pic-->

        <!-- START Background Caption-->
        <div class="bg-caption pull-up sm-pull-up text-white p-l-20 m-b-20">
          <h1 class="semi-bold text-white">Case Based Reasoning Menggunakan Algoritma KNN untuk Diagnosa Penyakit Lambung</h1>
          <p class="semi-bold text-white">Oleh : Andre Bhaskoro Suprayogi (11241010101045)</p>
        </div>
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
          <h2 class="semi-bold text-white">Program Studi Sistem Informasi</h2>
          <h1 class="semi-bold text-white">Universitas Jember</h1>
          <h1 class="semi-bold text-white">2017</h1>
        </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
          <!--
          <img src="<?= base_url() ?>assets/img/logo.png" alt="logo" data-src="<?= base_url() ?>assets/img/logo.png" data-src-retina="<?= base_url() ?>assets/img/logo_2x.png" width="78" height="22">
          -->

          <h1><?php echo lang('login_heading');?></h1>
          <h2><?php echo $this->config->item('nama_sistem') ?></h2>
          <span class="text-danger"><?php echo $message;?></span>
          <!--
          <p class="p-t-35">Sign into your pages account</p>
          -->


          <!-- START Login Form -->
          <?php echo form_open("auth/login");?>
            <div class="form-group form-group-default">
              <label>Login</label>
              <div class="controls">
                <?php echo form_input($identity);?>
              </div>
            </div>
            <div class="form-group form-group-default">
              <label>Password</label>
              <div class="controls">
                <?php echo form_input($password);?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 no-padding">
                <div class="checkbox ">
                  <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
                  <label for="remember">Keep Me Signed in</label>
                </div>
              </div>
            </div>
            <p>
            <button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
            <a  class="btn btn-success btn-cons m-t-10" href="<?php echo base_url()?>">Cancel</a>
          <?php echo form_close();?>
  
        </div>
      </div>
      <!-- END Login Right Container-->
    </div>
    <!-- BEGIN VENDOR JS -->
    <script src="<?= base_url() ?>assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="<?= base_url() ?>assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="<?= base_url() ?>assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/classie/classie.js"></script>
    <script src="<?= base_url() ?>assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <script src="<?= base_url() ?>assets/pages/js/pages.min.js"></script>
    <script>
    $(function()
    {
      $('#form-login').validate()
    })
    </script>
  </body>
</html>