<!-- START JUMBOTRON -->
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5><i class="fa fa-archive"></i> Detail Kasus <?php echo $row->kode; ?></h5>
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->
<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
            <p>Detail</p>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            
            <div class="row">
                    <table class="table">
                        <tr><td>User Input</td><td><?php echo $row->nama; ?> | <?php echo $row->email; ?></td></tr>
                        <tr><td>Kode Kasus</td><td><?php echo $row->kode; ?></td></tr>
                        <tr><td>Jenis Penyakit</td><td><?php echo $row->kode_penyakit; ?> - <?php echo $row->penyakit; ?></td></tr>
                        <tr>
                            <td>Gejala</td>
                            <td>
                                <ul>
                                <?php 
                                foreach ($dt_gejala as $d) {
                                    echo "<li>".$d->kode_gejala." - ".$d->gejala."</li>";
                                }
                                ?>
                                </ul>
                            </td>
                        </tr>
                        <tr><td>Pengobatan</td><td><?php echo $row->pengobatan; ?></td></tr>
                        <tr><td>Penyebab</td><td><?php echo $row->penyebab; ?></td></tr>
                        <tr><td>Keterangan</td><td><?php echo $row->keterangan; ?></td></tr>
                        <tr><td>Created At</td><td><?php echo $row->created_at; ?></td></tr>
                        <tr><td>Updated At</td><td><?php echo $row->updated_at; ?></td></tr>
                        <tr><td></td><td><a href="<?php echo site_url('basis_kasus') ?>" class="btn btn-info">Back</a></td></tr>
                    </table>
                </div>
        



        </div>
    </div>
</div>

