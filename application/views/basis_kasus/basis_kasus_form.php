<script src="<?= base_url('assets/js/ckeditor/ckeditor.js') ?>"></script>
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5><i class="fa fa-archive"></i> <?php echo $title ?></h5>
        </div>
    </div>
</div>

<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
                Form  Basis Kasus
            </div>
            <div class="panel-body">
                
                <form  role="form" action="<?php echo $action; ?>" method="post">
                <div class="row">
                    <div class="col-sm-5">
                        <input type="hidden" name="id_user" id="id_user"value="<?php echo $id_user; ?>" />
                        <div class="form-group">
                            <label for="varchar">Kode</label>
                            <input type="text" class="form-control" name="kode" id="kode" value="<?php echo $kode; ?>" readonly="readonly" />
                            <span class="text-danger"><?php echo form_error('kode') ?></span>
                        </div>
                        <div class="form-group">
                            <label for="varchar">Jenis Penyakit Lambung</label><br> 
                            <select name="kode_penyakit" id="kode_penyakit" class="cs-select cs-skin-slide" data-init-plugin="cs-select">
                                <option value="">Pilih... </option>
                                <?php
                                foreach ($dt_penyakit as $dt) {
                                    echo '<option value="'.$dt->kode.'"'; 
                                    if($kode_penyakit == $dt->kode ){echo "selected";} 
                                    echo '>'.$dt->kode.' '.$dt->penyakit.'</option>';
                                }
                                ?>
                            </select>
                            <span class="text-danger"><?php echo form_error('kode_penyakit') ?></span>
                        </div>
                        <div class="form-group">
                            <label for="penyebab">Penyebab</label>
                            <textarea class="form-control" rows="3" name="penyebab" id="penyebab" placeholder="Penyebab"><?php echo $penyebab; ?></textarea>
                            <span class="text-danger"><?php echo form_error('penyebab') ?></span>
                        </div>
                        <div class="form-group">
                            <label for="pengobatan">Pengobatan</label>
                            <textarea class="form-control" rows="3" name="pengobatan" id="pengobatan" placeholder="Pengobatan"><?php echo $pengobatan; ?></textarea>
                            <span class="text-danger"><?php echo form_error('pengobatan') ?></span>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <textarea class="form-control" rows="3" name="keterangan" id="keterangan" placeholder="Keterangan"><?php echo $keterangan; ?></textarea>
                            <span class="text-danger"><?php echo form_error('keterangan') ?></span>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 

                    </div>
                    <div class="col-sm-7">
                        <h3>Pilih Gejala</h3>
                        <span class="text-danger"><?php echo form_error('gejala[]') ?></span>
                        <?php 
                        foreach ($dt_gejala as $d) {
                            if($button == "Update") 
                            {
                                $this->db->where('kode_kasus', $kode);
                                $this->db->where('kode_gejala', $d->kode);                             
                                echo '
                                <div class="checkbox check-primary">
                                    <input ';
                                if($this->db->get('kasus_detail')->row())
                                {
                                    echo 'checked="checked"';
                                } 
                                echo ' type="checkbox" name="gejala[]" value="'.$d->kode.'" id="'.$d->kode.'">
                                    <label for="'.$d->kode.'">'.$d->kode.' - '.$d->gejala.'</label>
                                </div>
                                ';
                            }
                            else 
                            {
                                echo '
                                <div class="checkbox check-primary">
                                    <input type="checkbox" name="gejala[]" value="'.$d->kode.'" id="'.$d->kode.'">
                                    <label for="'.$d->kode.'">'.$d->kode.' - '.$d->gejala.'</label>
                                </div>
                                ';
                            }
                           
                        }
                        
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <p>I hereby certify that the information above is true and accurate. </p>
                    </div>
                    <div class="col-sm-9">
                        <button class="btn btn-success" type="submit">Submit</button>
                        <a href="<?php echo site_url('basis_kasus') ?>" class="btn btn-default">Cancel</a>
                    </div>
                </div>
                </form>

            </div>

        </div>
    </div>
</div> 
<script type="text/javascript">
    CKEDITOR.replace( 'penyebab' );
CKEDITOR.replace( 'pengobatan' );
CKEDITOR.replace( 'keterangan' );
</script>