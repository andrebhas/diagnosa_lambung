<!-- START JUMBOTRON -->
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5><i class="fa fa-archive"></i> Basis Kasus Diagnosa Lambung</h5>
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->
<div class="row" style="margin-bottom: 10px">
    <div class="col-md-12 text-center">
        <div style="margin-top: 4px"  id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        </div>
    </div>
</div>
<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
            <?php //echo anchor(site_url('basis_kasus/create'), '<i class="fa fa-plus-square"></i><span class="bold"> Create New</span>', 'class="btn btn-success btn-cons btn-xs"'); ?>
		    <?php //echo anchor(site_url('basis_kasus/excel'), 'Excel', 'class="btn btn-warning btn-xs"'); ?></div>
            <div class="pull-right">
                <div class="col-xs-12">
                <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped dataTable no-footer" id="table-basis_kasus">
                <thead>
                    <tr>
                        <th width="50px">No</th>
					    <th width="50px">Kode</th>
					    <th>Pengobatan</th>
					    <th>Penyebab</th>
					    <th>Keterangan</th>
					    <th>Penyakit</th>
					    <th width="200px">Action</th>
                    </tr>
                </thead>
				<tbody>
                <?php
                    $start = 0;
                    foreach ($basis_kasus_data as $basis_kasus)
                    {
                ?>
                    <tr>
						<td><?php echo ++$start ?></td>
						<td><?php echo $basis_kasus->kode ?></td>
						<td><?php echo character_limiter($basis_kasus->pengobatan,50); ?></td>
						<td><?php echo character_limiter($basis_kasus->penyebab,50); ?></td>
						<td><?php echo character_limiter($basis_kasus->keterangan,50); ?></td>
						<td><?php echo $this->Penyakit_model->get_by_kode($basis_kasus->kode_penyakit)->penyakit;   ?></td>
						<td style="text-align:center" width="200px">
						<?php 
                            echo anchor(site_url('basis_kasus/read/'.$basis_kasus->id),'<i class="fa fa-eye"></i> Detail', 'class="btn btn-xs btn-info"'); 
                            // if($user->id == $basis_kasus->id_user)
                            // {
                                echo anchor(site_url('basis_kasus/update/'.$basis_kasus->id),'<i class="fa fa-edit"></i> Edit', 'class="btn btn-xs btn-warning"'); 
							    echo anchor(site_url('basis_kasus/delete/'.$basis_kasus->id),'<i class="fa fa-trash"></i> Delete','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                            // }							
						?>
						</td>
					</tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
