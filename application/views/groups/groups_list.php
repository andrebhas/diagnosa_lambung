<!-- START JUMBOTRON -->
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5>Groups List</h5>
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->
<div class="row" style="margin-bottom: 10px">
    <div class="col-md-12 text-center">
        <div style="margin-top: 4px"  id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        </div>
    </div>
</div>
<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
            <?php echo anchor(site_url('groups/create'), '<i class="fa fa-plus-square"></i><span class="bold"> Create New</span>', 'class="btn btn-success btn-cons btn-xs"'); ?></div>
            <div class="pull-right">
                <div class="col-xs-12">
                <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped dataTable no-footer" id="table-groups">
                <thead>
                    <tr>
                        <th width="80px">No</th>
					    <th>Name</th>
					    <th>Description</th>
					    <th width="200px">Action</th>
                    </tr>
                </thead>
				<tbody>
                <?php
                    $start = 0;
                    foreach ($groups_data as $groups)
                    {
                ?>
                    <tr>
						<td><?php echo ++$start ?></td>
						<td><?php echo $groups->name ?></td>
						<td><?php echo $groups->description ?></td>
						<td style="text-align:center" width="200px">
						<?php 
							//echo anchor(site_url('groups/read/'.$groups->id),'<i class="fa fa-eye"></i>', 'class="btn btn-xs btn-info"'); 
							echo anchor(site_url('groups/update/'.$groups->id),'<i class="fa fa-edit"></i> Edit', 'class="btn btn-xs btn-warning"'); 
							echo anchor(site_url('groups/delete/'.$groups->id),'<i class="fa fa-trash"></i> Delete','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
						?>
						</td>
					</tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
