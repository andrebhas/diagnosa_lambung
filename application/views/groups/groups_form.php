
<div class="jumbotron" >
    <div class="container-fluid">
        <div class="inner">
            <h5>Groups</h5>
        </div>
    </div>
</div>

<div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">
                Form <?= $button ?> Groups
            </div>
            <div class="panel-body">
                <div class="row">
                  <div class="col-sm-10">



                <form  class="form-horizontal" role="form" action="<?php echo $action; ?>" method="post">
				    <div class="form-group">
                        <label for="varchar" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">    
                            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                            <span class="text-danger"><?php echo form_error('name') ?></span>
                        </div>
                    </div>
				    <div class="form-group">
                        <label for="varchar" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-9">    
                            <input type="text" class="form-control" name="description" id="description" placeholder="Description" value="<?php echo $description; ?>" />
                            <span class="text-danger"><?php echo form_error('description') ?></span>
                        </div>
                    </div>
				    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 

                    <div class="row"><br>
                        <div class="col-sm-3">
                          <p>I hereby certify that the information above is true and accurate. </p>
                        </div>
                        <div class="col-sm-9">
                          <button class="btn btn-success" type="submit">Submit</button>
                          <a href="<?php echo site_url('groups') ?>" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
				</form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 