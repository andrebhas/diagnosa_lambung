<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Auto-load Packages
| -------------------------------------------------------------------
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
*/
$autoload['packages'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
|	$autoload['libraries'] = array('database', 'email', 'session');
*/
$autoload['libraries'] = array('database','session','form_validation','ion_auth','datatables');

/*
| -------------------------------------------------------------------
|  Auto-load Drivers
| -------------------------------------------------------------------
|	$autoload['drivers'] = array('cache');
*/
$autoload['drivers'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
|	$autoload['helper'] = array('url', 'file');
*/
$autoload['helper'] = array('url', 'file', 'string', 'text');

/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
|	$autoload['config'] = array('config1', 'config2');
*/
$autoload['config'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
|	$autoload['language'] = array('lang1', 'lang2');
*/
$autoload['language'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
|	$autoload['model'] = array('first_model', 'second_model');
*/
$autoload['model'] = array();
