<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Basis_kasus extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Basis_kasus_model');
        $this->load->model('Penyakit_model');
        $this->load->model('Gejala_model');
        if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
    }

    public function index()
    {
        $basis_kasus = $this->Basis_kasus_model->get_all();
        $user = $this->ion_auth->user()->row();
		$data = array(
			'content' => 'basis_kasus/basis_kasus_list' ,
            'js_script' => 'basis_kasus/basis_kasus_js_script',
            'basis_kasus_data' => $basis_kasus,
			'user' => $user ,
		);	
		$this->load->view('layout/template', $data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Basis_kasus_model->json();
    }

    public function read($id) 
    {
        $user = $this->ion_auth->user()->row();
        $row = $this->Basis_kasus_model->get_by_id($id);
        if ($row) {
            $gejala = $this->Basis_kasus_model->get_gejala($row->kode);
            $data = array(
                'js_script' => 'basis_kasus/basis_kasus_js_script',
                'row' => $row ,
			    'content' => 'basis_kasus/basis_kasus_read' ,
				'user' => $user ,
                'dt_gejala' => $gejala ,
            );
            $this->load->view('layout/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('basis_kasus'));
        }
    }

    public function create() 
    {
        $user = $this->ion_auth->user()->row();
        $date = new DateTime();
		$timestamp = $date->format('U');
        $kode = "K".$timestamp;
        $dt_penyakit = $this->Penyakit_model->get_all();
        $dt_gejala = $this->Gejala_model->get_all();
        $data = array(
            'content' => 'basis_kasus/basis_kasus_form' ,
            'js_script' => 'basis_kasus/basis_kasus_js_script',
            'title' => 'Create Basis Kasus',
			'user' => $user ,
            'button' => 'Create',
            'action' => site_url('basis_kasus/create_action'),
		    'id' => set_value('id'),
		    'id_user' => $user->id,
		    'kode' => $kode,
		    'pengobatan' => set_value('pengobatan'),
		    'penyebab' => set_value('penyebab'),
		    'keterangan' => set_value('keterangan'),
		    'kode_penyakit' => set_value('kode_penyakit'),
            'dt_penyakit' => $dt_penyakit ,
            'dt_gejala' => $dt_gejala ,
		);
        $this->load->view('layout/template', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $kode = $this->input->post('kode',TRUE);
            $gejala = $this->input->post('gejala',TRUE);
            $data = array(
				'id_user' => $this->input->post('id_user',TRUE),
				'kode' => $kode,
				'pengobatan' => $this->input->post('pengobatan',TRUE),
				'penyebab' => $this->input->post('penyebab',TRUE),
				'keterangan' => $this->input->post('keterangan',TRUE),
				'kode_penyakit' => $this->input->post('kode_penyakit',TRUE),
		    );
            $this->Basis_kasus_model->insert($data);
            foreach ($gejala as $g) {
                $this->db->insert('kasus_detail', array('kode_kasus' => $kode , 'kode_gejala' => $g ));
            }
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('basis_kasus'));
        }
    }
    
    public function update($id) 
    {
        $user = $this->ion_auth->user()->row();
        $row = $this->Basis_kasus_model->get_by_id($id);
        if ($row) {
            $dt_penyakit = $this->Penyakit_model->get_all();
            $dt_gejala = $this->Gejala_model->get_all();
            $data = array(
                'js_script' => 'basis_kasus/basis_kasus_js_script',
                'content' => 'basis_kasus/basis_kasus_form' ,
                'title' => 'Update Basis Kasus',
                'user' => $user,
                'button' => 'Update',
                'action' => site_url('basis_kasus/update_action'),
				'id' => set_value('id', $row->id),
				'id_user' => set_value('id_user', $row->id_user),
				'kode' => set_value('kode', $row->kode),
				'pengobatan' => set_value('pengobatan', $row->pengobatan),
				'penyebab' => set_value('penyebab', $row->penyebab),
				'keterangan' => set_value('keterangan', $row->keterangan),
				'kode_penyakit' => set_value('kode_penyakit', $row->kode_penyakit),
                'dt_penyakit' => $dt_penyakit ,
                'dt_gejala' => $dt_gejala ,
            );
            $this->load->view('layout/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('basis_kasus'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $date = new DateTime();
		    $timestamp = $date->format('Y-m-d H:i:s');
            $kode = $this->input->post('kode',TRUE);
            $gejala = $this->input->post('gejala',TRUE);
            $data = array(
				'id_user' => $this->input->post('id_user',TRUE),
				'kode' => $kode,
				'pengobatan' => $this->input->post('pengobatan',TRUE),
				'penyebab' => $this->input->post('penyebab',TRUE),
				'keterangan' => $this->input->post('keterangan',TRUE),
				'kode_penyakit' => $this->input->post('kode_penyakit',TRUE),
				'updated_at' => $timestamp,
		    );
            $this->db->where('kode_kasus', $kode);
            $this->db->delete('kasus_detail');
            $this->Basis_kasus_model->update($this->input->post('id', TRUE), $data);
            foreach ($gejala as $g) {
                $this->db->insert('kasus_detail', array('kode_kasus' => $kode , 'kode_gejala' => $g ));
            }
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('basis_kasus'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Basis_kasus_model->get_by_id($id);
        if ($row) {
            $this->Basis_kasus_model->delete($id);
            $this->db->where('kode_kasus', $row->kode);
            $this->db->delete('kasus_detail');
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('basis_kasus'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('basis_kasus'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
		$this->form_validation->set_rules('kode', 'kode', 'trim|required');
		$this->form_validation->set_rules('pengobatan', 'pengobatan', 'trim|required');
		$this->form_validation->set_rules('penyebab', 'penyebab', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
		$this->form_validation->set_rules('kode_penyakit', 'kode penyakit', 'trim|required');
        $this->form_validation->set_rules('gejala[]','gejala','trim|required');
		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "\data-basis_kasus.xls";
        $judul = "Data basis_kasus";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");
        xlsBOF();
        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Id User");
		xlsWriteLabel($tablehead, $kolomhead++, "Kode");
		xlsWriteLabel($tablehead, $kolomhead++, "Pengobatan");
		xlsWriteLabel($tablehead, $kolomhead++, "Penyebab");
		xlsWriteLabel($tablehead, $kolomhead++, "Keterangan");
		xlsWriteLabel($tablehead, $kolomhead++, "Kode Penyakit");
		xlsWriteLabel($tablehead, $kolomhead++, "Created At");
		xlsWriteLabel($tablehead, $kolomhead++, "Updated At");

		foreach ($this->Basis_kasus_model->get_all() as $data) {
            $kolombody = 0;
            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
		    xlsWriteNumber($tablebody, $kolombody++, $gejala);
		    xlsWriteLabel($tablebody, $kolombody++, $data->kode);
		    xlsWriteLabel($tablebody, $kolombody++, $data->pengobatan);
		    xlsWriteLabel($tablebody, $kolombody++, $data->penyebab);
		    xlsWriteLabel($tablebody, $kolombody++, $data->keterangan);
		    xlsWriteLabel($tablebody, $kolombody++, $data->kode_penyakit);
		    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
		    xlsWriteLabel($tablebody, $kolombody++, $data->updated_at);

		    $tablebody++;
            $nourut++;
        }
        xlsEOF();
        exit();
    }

}

/* Location: ./application/controllers/Basis_kasus.php */
/* Please DO NOT modify this information : */
/* Kode ini di buat oleh Andre Bhaskoro pada tgl 2017-06-19 05:16:26 */
/* http://amertaproject.com | +6282333817317 */