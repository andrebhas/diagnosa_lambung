<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct(); 
        $this->load->model('Basis_kasus_model');
        $this->load->model('Gejala_model');
        $this->load->model('Kasus_baru_model');
        $this->load->model('Penyakit_model');
    }

    public function index()
    {
        $user = $this->ion_auth->user()->row();
		$data = array(
			'content' => 'dashboard/v_dashboard' ,
            'js_script' => 'dashboard/_js_script',
			'user' => $user ,
		);	
		$this->load->view('layout/template', $data);
    } 

}

/* Location: ./application/controllers/dashboard.php */
/* Please DO NOT modify this information : */
/* Kode ini di buat oleh Andre Bhaskoro pada tgl 2017-06-09 22:06:54 */
/* http://amertaproject.com | +6282333817317 */