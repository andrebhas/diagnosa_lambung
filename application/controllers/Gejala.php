<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gejala extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Gejala_model');
        if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
    }

    public function index()
    {
        $gejala = $this->Gejala_model->get_all();
        $user = $this->ion_auth->user()->row();
		$data = array(
			'content' => 'gejala/gejala_list' ,
            'js_script' => 'gejala/gejala_js_script',
            'gejala_data' => $gejala,
			'user' => $user ,
		);	
		$this->load->view('layout/template', $data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Gejala_model->json();
    }

    public function read($id) 
    {
        $user = $this->ion_auth->user()->row();
        $row = $this->Gejala_model->get_by_id($id);
        if ($row) {
            $data = array(
                'js_script' => 'gejala/gejala_js_script',
				'id' => $row->id,
				'kode' => $row->kode,
				'gejala' => $row->gejala,
				'bobot' => $row->bobot,
			    'content' => 'gejala/gejala_read' ,
				'user' => $user ,
            );
            $this->load->view('layout/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('gejala'));
        }
    }

    public function create() 
    {
        $user = $this->ion_auth->user()->row();
        $first_row = $this->Gejala_model->get_first_row();
        if (!$first_row ) {
            $kode = "G1";
        } else {
            $kodenya = $first_row->kode;
            $pisah = explode('G',$kodenya);
            $num = $pisah[1] + 1;
            $kode = "G".$num;
        }
        $data = array(
            'content' => 'gejala/gejala_form' ,
            'js_script' => 'gejala/gejala_js_script',
			'user' => $user ,
            'button' => 'Create',
            'action' => site_url('gejala/create_action'),
		    'id' => set_value('id'),
		    'kode' => $kode,
		    'gejala' => set_value('gejala'),
		    'bobot' => set_value('bobot'),
		);
        $this->load->view('layout/template', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
				'kode' => $this->input->post('kode',TRUE),
				'gejala' => $this->input->post('gejala',TRUE),
				'bobot' => $this->input->post('bobot',TRUE),
		    );
            $this->Gejala_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('gejala'));
        }
    }
    
    public function update($id) 
    {
        $user = $this->ion_auth->user()->row();
        $row = $this->Gejala_model->get_by_id($id);
        if ($row) {
            $data = array(
                'js_script' => 'gejala/gejala_js_script',
                'content' => 'gejala/gejala_form' ,
                'user' => $user,
                'button' => 'Update',
                'action' => site_url('gejala/update_action'),
				'id' => set_value('id', $row->id),
				'kode' => set_value('kode', $row->kode),
				'gejala' => set_value('gejala', $row->gejala),
				'bobot' => set_value('bobot', $row->bobot),
		    );
            $this->load->view('layout/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('gejala'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
				'kode' => $this->input->post('kode',TRUE),
				'gejala' => $this->input->post('gejala',TRUE),
				'bobot' => $this->input->post('bobot',TRUE),
		    );
            $this->Gejala_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('gejala'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Gejala_model->get_by_id($id);
        if ($row) {
            $this->Gejala_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('gejala'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('gejala'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('kode', 'kode', 'trim|required');
		$this->form_validation->set_rules('gejala', 'gejala', 'trim|required');
		$this->form_validation->set_rules('bobot', 'bobot', 'trim|required|numeric');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* Location: ./application/controllers/Gejala.php */
/* Please DO NOT modify this information : */
/* Kode ini di buat oleh Andre Bhaskoro pada tgl 2017-06-18 09:35:49 */
/* http://amertaproject.com | +6282333817317 */