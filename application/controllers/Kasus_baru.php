<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kasus_baru extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kasus_baru_model');
        $this->load->model('Basis_kasus_model');
        $this->load->model('Gejala_model');
        $this->load->model('Penyakit_model');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
        $kasus_baru = $this->Kasus_baru_model->get_all();
        $user = $this->ion_auth->user()->row();
		$data = array(
			'content' => 'kasus_baru/kasus_baru_list' ,
            'js_script' => 'kasus_baru/kasus_baru_js_script',
            'kasus_baru_data' => $kasus_baru,
			'user' => $user ,
		);	
		$this->load->view('layout/template', $data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Kasus_baru_model->json();
    }

    public function read($id) 
    {
        $user = $this->ion_auth->user()->row();
        $row = $this->Kasus_baru_model->get_by_id($id);
        if ($row) {
            $gejala = $this->Basis_kasus_model->get_gejala($row->kode);
            $data = array(
                'js_script' => 'kasus_baru/kasus_baru_js_script',
				'id' => $row->id,
				'kode' => $row->kode,
				'nama' => $row->nama,
				'usia' => $row->usia,
				'latar_belakang' => $row->latar_belakang,
				'nilai_knn' => $row->nilai_knn,
                'kode_similaritas' => $row->kode_similaritas,
				'status' => $row->status,
				'created_at' => $row->created_at,
				'updated_at' => $row->updated_at,
			    'content' => 'kasus_baru/kasus_baru_read' ,
				'user' => $user ,
                'gejala' => $gejala ,
            );
            $this->load->view('layout/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kasus_baru'));
        }
    }

    public function json_get_gejala($kode)
    {
        $this->db->where('kode_kasus', $kode);
        $query = $this->db->get('v_kasus_detail');
        echo json_encode($query->result());
    }

    public function create() {
        $user = $this->ion_auth->user()->row();
        $date = new DateTime();
        $first_row_kasus_baru = $this->Kasus_baru_model->get_first_row();
        $first_row_basis_kasus = $this->Basis_kasus_model->get_first_row();
        if($first_row_kasus_baru){
            $kodenya = $first_row_kasus_baru->kode;
            $pisah = explode('K',$kodenya);
            $num = $pisah[1] + 1;
            $kode = "K".$num;
        } else {
            $kodenya = $first_row_basis_kasus->kode;
            $pisah = explode('K',$kodenya);
            $num = $pisah[1] + 1;
            $kode = "K".$num;
        }
        echo $kode;
        $dt_gejala = $this->Gejala_model->get_all_asc();
        $data = array(
           'content' => 'kasus_baru/kasus_baru_form' ,
            'js_script' => 'kasus_baru/kasus_baru_js_script',
			'user' => $user ,
            'button' => 'Create',
            'action' => site_url('kasus_baru/hasil_diagnosa'),
		    'id' => set_value('id'),
		    'kode' => $kode,
		    'nama' => set_value('nama'),
		    'usia' => set_value('usia'),
		    'latar_belakang' => set_value('latar_belakang'),
            'dt_gejala' => $dt_gejala ,
		);
        $this->load->view('layout/template', $data);
    }

    public function hasil_diagnosa()
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $kode = $this->input->post('kode',TRUE);
            $gejala = $this->input->post('gejala',TRUE);
            $basis_kasus = $this->Basis_kasus_model->get_all();
            foreach ($gejala as $gj) {
               $nilai_input[] = $this->Gejala_model->get_by_kode($gj)->bobot;
            }
            $nilai_inputan = array_sum($nilai_input);
            $total = array();
            foreach ($basis_kasus as $bk) {   
                $kasus_detail = $this->Basis_kasus_model->get_gejala($bk->kode);
                foreach ($kasus_detail as $kd) {
                    foreach ($gejala as $g) {
                        if($kd->kode_gejala == $g){
                            $bobot[$kd->kode_kasus][$g] = $kd->bobot; 
                        } else {
                            $bobot[$kd->kode_kasus]['G0'] = 0;
                        }
                    }
                } 
            }
            foreach ($basis_kasus as $bk) {
                 if($bobot[$bk->kode]){
                    $total[$bk->kode] = array_sum($bobot[$bk->kode]);
                 } else {
                    $total[$bk->kode] = 0;
                 }
            }
            foreach ($bobot as $key => $value) {
                $hasil_bagi = $total[$key] / $nilai_inputan ;
                $persen_hasil_bagi = $hasil_bagi * 100;
                $arr_hasil[] = $persen_hasil_bagi."-".$key;
            } 
            $count = count($arr_hasil);
            $temp_kode_nilai = array();
            for ($i=0; $i < $count; $i++) { 
                $hasil = explode('-',$arr_hasil[$i]);
                $temp_kode_nilai[$hasil[1]] = $hasil[0];      
            }
            $maxs = max($temp_kode_nilai);
            $kode_same_val = array_keys($temp_kode_nilai, max($temp_kode_nilai));
            for ($i=0; $i < count($kode_same_val) ; $i++) { 
                $temp_nilai[] = $maxs;
            }
            $hasil_filnal_diagnosa = array_combine($kode_same_val, $temp_nilai); 

            $user = $this->ion_auth->user()->row();
            $data = array(
                'user' => $user ,
                'content' => 'kasus_baru/v_hasil_diagnosa' ,
                'js_script' => 'kasus_baru/kasus_baru_js_script',
                'action' => site_url('kasus_baru/create_action'),
				'kode' => $kode,
				'nama' => $this->input->post('nama',TRUE),
				'usia' => $this->input->post('usia',TRUE),
				//'latar_belakang' => $this->input->post('latar_belakang',TRUE),
                'gejala' => $gejala ,
                'hasil_diagnosa' =>  $hasil_filnal_diagnosa,
		    );
            $this->load->view('layout/template', $data);
        }
    }
    
    public function create_action() 
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $kode = $this->input->post('kode',TRUE);
            $gejala = $this->input->post('gejala',TRUE);            
            $data = array(
				'kode' => $kode,
				'nama' => $this->input->post('nama',TRUE),
				'usia' => $this->input->post('usia',TRUE),
				//'latar_belakang' => $this->input->post('latar_belakang',TRUE),
                'nilai_knn' => $this->input->post('nilai_knn',TRUE),
                'kode_similaritas' => $this->input->post('kode_similaritas',TRUE) ,
		    );
            $this->Kasus_baru_model->insert($data);
            foreach ($gejala as $g) {
                $this->db->insert('kasus_detail', array('kode_kasus' => $kode , 'kode_gejala' => $g ));
            }
            
            if (!$this->ion_auth->logged_in())
            {
                $this->session->set_flashdata('message', 'Create Record Success');
                redirect('dashboard', 'refresh');
            } else {
                $this->session->set_flashdata('message', 'Create Record Success');
                redirect(site_url('kasus_baru'));
            }
        }
    }
    
    public function revise($id_kasus_baru) 
    {
        if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
        $user = $this->ion_auth->user()->row();
        $row = $this->Kasus_baru_model->get_by_id($id_kasus_baru);
        if($row){
            $kode_kasus = $row->kode;
            $kode_penyakit = $row->kode_similaritas;
            $gejala = $this->Basis_kasus_model->get_gejala($row->kode);
            $data = array(
                'js_script' => 'kasus_baru/kasus_baru_js_script',
                'content' => 'kasus_baru/v_revise_form' ,
                'action' => site_url('kasus_baru/revise_action/'.$id_kasus_baru),
                'user' => $user,
                'id_user' => $user->id ,
                'kode_kasus' => $kode_kasus ,
                'kode_penyakit' => $kode_penyakit,
                'gejala' => $gejala,
                'id_user' => $user->id,
                'pengobatan' => set_value('pengobatan'),
                'penyebab' => set_value('penyebab'),
                'keterangan' => set_value('keterangan'),
                'id_kasus_baru' => $id_kasus_baru,
            );
            $this->load->view('layout/template', $data);
        } else {
            echo "data tidak ditemukan";
        }
    }
    
    public function revise_action($id_kasus_baru) 
    {
        $this->revise_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->revise($id_kasus_baru);
        } else {
            $kode = $this->input->post('kode',TRUE);
            $gejala = $this->input->post('gejala',TRUE);
            $data = array(
				'id_user' => $this->input->post('id_user',TRUE),
				'kode' => $kode,
				'pengobatan' => $this->input->post('pengobatan',TRUE),
				'penyebab' => $this->input->post('penyebab',TRUE),
				'keterangan' => $this->input->post('keterangan',TRUE),
				'kode_penyakit' => $this->input->post('kode_penyakit',TRUE),
		    );
            $this->Basis_kasus_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('basis_kasus'));
        }
    }

    public function revise_rules() 
    {
		$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
		$this->form_validation->set_rules('kode', 'kode', 'trim|required');
		$this->form_validation->set_rules('pengobatan', 'pengobatan', 'trim|required');
		$this->form_validation->set_rules('penyebab', 'penyebab', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
		$this->form_validation->set_rules('kode_penyakit', 'kode penyakit', 'trim|required');
		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
    
    public function delete($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
        $row = $this->Kasus_baru_model->get_by_id($id);
        if ($row) {
            //$this->Kasus_baru_model->delete($id);
            $data = array(
				'deleted' => 1,
		    );
            $this->Kasus_baru_model->update($id, $data);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('kasus_baru'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kasus_baru'));
        }
    }

    public function tambah_penyakit_action($id_kasus_baru) 
    {
        $data = array(
            'kode' => $this->input->post('kode',TRUE),
            'penyakit' => $this->input->post('penyakit',TRUE),
        );
        $this->Penyakit_model->insert($data);
        redirect(site_url('kasus_baru/revise/'.$id_kasus_baru));
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('kode', 'kode', 'trim|required');
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('usia', 'usia', 'trim|required|numeric');
		//$this->form_validation->set_rules('latar_belakang', 'latar belakang', 'trim|required');
        $this->form_validation->set_rules('gejala[]','gejala','trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "\data-kasus_baru.xls";
        $judul = "Data kasus_baru";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");
        xlsBOF();
        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Kode");
		xlsWriteLabel($tablehead, $kolomhead++, "Nama");
		xlsWriteLabel($tablehead, $kolomhead++, "Usia");
		xlsWriteLabel($tablehead, $kolomhead++, "Latar Belakang");
		xlsWriteLabel($tablehead, $kolomhead++, "Nilai Knn");
		xlsWriteLabel($tablehead, $kolomhead++, "Status");
		xlsWriteLabel($tablehead, $kolomhead++, "Created At");
		xlsWriteLabel($tablehead, $kolomhead++, "Updated At");

		foreach ($this->Kasus_baru_model->get_all() as $data) {
            $kolombody = 0;
            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
		    xlsWriteLabel($tablebody, $kolombody++, $data->kode);
		    xlsWriteLabel($tablebody, $kolombody++, $data->nama);
		    xlsWriteNumber($tablebody, $kolombody++, $data->usia);
		    xlsWriteLabel($tablebody, $kolombody++, $data->latar_belakang);
		    xlsWriteNumber($tablebody, $kolombody++, $data->nilai_knn);
		    xlsWriteLabel($tablebody, $kolombody++, $data->status);
		    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
		    xlsWriteLabel($tablebody, $kolombody++, $data->updated_at);

		    $tablebody++;
            $nourut++;
        }
        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=kasus_baru.doc");
        $data = array(
            'kasus_baru_data' => $this->Kasus_baru_model->get_all(),
            'start' => 0
        );
        $this->load->view('kasus_baru/kasus_baru_doc',$data);
    }

}

/* Location: ./application/controllers/Kasus_baru.php */
/* Please DO NOT modify this information : */
/* Kode ini di buat oleh Andre Bhaskoro pada tgl 2017-06-20 07:08:08 */
/* http://amertaproject.com | +6282333817317 */