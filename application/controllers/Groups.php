<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Groups extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Groups_model');
        if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
    }

    public function index()
    {
        $groups = $this->Groups_model->get_all();
        $user = $this->ion_auth->user()->row();
		$data = array(
			'content' => 'groups/groups_list' ,
            'js_script' => 'groups/groups_js_script',
            'groups_data' => $groups,
			'user' => $user ,
		);	
		$this->load->view('layout/template', $data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Groups_model->json();
    }

    public function read($id) 
    {
        $user = $this->ion_auth->user()->row();
        $row = $this->Groups_model->get_by_id($id);
        if ($row) {
            $data = array(
                'js_script' => 'groups/groups_js_script',
				'id' => $row->id,
				'name' => $row->name,
				'description' => $row->description,
			    'content' => 'groups/groups_read' ,
				'user' => $user ,
            );
            $this->load->view('layout/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('groups'));
        }
    }

    public function create() 
    {
        $user = $this->ion_auth->user()->row();
        $data = array(
            'content' => 'groups/groups_form' ,
            'js_script' => 'groups/groups_js_script',
			'user' => $user ,
            'button' => 'Create',
            'action' => site_url('groups/create_action'),
		    'id' => set_value('id'),
		    'name' => set_value('name'),
		    'description' => set_value('description'),
		);
        $this->load->view('layout/template', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
				'name' => $this->input->post('name',TRUE),
				'description' => $this->input->post('description',TRUE),
		    );
            $this->Groups_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('groups'));
        }
    }
    
    public function update($id) 
    {
        $user = $this->ion_auth->user()->row();
        $row = $this->Groups_model->get_by_id($id);
        if ($row) {
            $data = array(
                'js_script' => 'groups/groups_js_script',
                'content' => 'groups/groups_form' ,
                'user' => $user,
                'button' => 'Update',
                'action' => site_url('groups/update_action'),
				'id' => set_value('id', $row->id),
				'name' => set_value('name', $row->name),
				'description' => set_value('description', $row->description),
		    );
            $this->load->view('layout/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('groups'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
				'name' => $this->input->post('name',TRUE),
				'description' => $this->input->post('description',TRUE),
		    );
            $this->Groups_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('groups'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Groups_model->get_by_id($id);
        if ($row) {
            $this->Groups_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('groups'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('groups'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$this->form_validation->set_rules('description', 'description', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* Location: ./application/controllers/Groups.php */
/* Please DO NOT modify this information : */
/* Kode ini di buat oleh Andre Bhaskoro pada tgl 2017-06-10 19:50:05 */
/* http://amertaproject.com | +6282333817317 */