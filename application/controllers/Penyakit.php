<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penyakit extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penyakit_model');
        if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
    }

    public function index()
    {
        $penyakit = $this->Penyakit_model->get_all();
        $user = $this->ion_auth->user()->row();
		$data = array(
			'content' => 'penyakit/penyakit_list' ,
            'js_script' => 'penyakit/penyakit_js_script',
            'penyakit_data' => $penyakit,
			'user' => $user ,
		);	
		$this->load->view('layout/template', $data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Penyakit_model->json();
    }

    public function read($id) 
    {
        $user = $this->ion_auth->user()->row();
        $row = $this->Penyakit_model->get_by_id($id);
        if ($row) {
            $data = array(
                'js_script' => 'penyakit/penyakit_js_script',
				'id' => $row->id,
				'kode' => $row->kode,
				'penyakit' => $row->penyakit,
			    'content' => 'penyakit/penyakit_read' ,
				'user' => $user ,
            );
            $this->load->view('layout/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penyakit'));
        }
    }

    public function create() 
    {
        $user = $this->ion_auth->user()->row();
        $first_row = $this->Penyakit_model->get_first_row();
        if (!$first_row ) {
            $kode = "P1";
        } else {
            $kodenya = $first_row->kode;
            $pisah = explode('P',$kodenya);
            $num = $pisah[1] + 1;
            $kode = "P".$num;
        }
        
        $data = array(
            'content' => 'penyakit/penyakit_form' ,
            'js_script' => 'penyakit/penyakit_js_script',
			'user' => $user ,
            'button' => 'Create',
            'action' => site_url('penyakit/create_action'),
		    'id' => set_value('id'),
		    'kode' => $kode,
		    'penyakit' => set_value('penyakit'),
		);
        $this->load->view('layout/template', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
				'kode' => $this->input->post('kode',TRUE),
				'penyakit' => $this->input->post('penyakit',TRUE),
		    );
            $this->Penyakit_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('penyakit'));
        }
    }
    
    public function update($id) 
    {
        $user = $this->ion_auth->user()->row();
        $row = $this->Penyakit_model->get_by_id($id);
        if ($row) {
            $data = array(
                'js_script' => 'penyakit/penyakit_js_script',
                'content' => 'penyakit/penyakit_form' ,
                'user' => $user,
                'button' => 'Update',
                'action' => site_url('penyakit/update_action'),
				'id' => set_value('id', $row->id),
				'kode' => set_value('kode', $row->kode),
				'penyakit' => set_value('penyakit', $row->penyakit),
		    );
            $this->load->view('layout/template', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penyakit'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
				'kode' => $this->input->post('kode',TRUE),
				'penyakit' => $this->input->post('penyakit',TRUE),
		    );
            $this->Penyakit_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('penyakit'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Penyakit_model->get_by_id($id);
        if ($row) {
            $this->Penyakit_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('penyakit'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penyakit'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('kode', 'kode', 'trim|required');
		$this->form_validation->set_rules('penyakit', 'penyakit', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* Location: ./application/controllers/Penyakit.php */
/* Please DO NOT modify this information : */
/* Kode ini di buat oleh Andre Bhaskoro pada tgl 2017-06-18 09:35:57 */
/* http://amertaproject.com | +6282333817317 */