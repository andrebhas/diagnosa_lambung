<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Basis_kasus_model extends CI_Model
{
    public $table = 'basis_kasus';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id,id_user,kode,pengobatan,penyebab,keterangan,kode_penyakit,created_at,updated_at');
        $this->datatables->from('basis_kasus');
        $this->datatables->add_column('action', anchor(site_url('basis_kasus/read/$1'),'Read')." | ".anchor(site_url('basis_kasus/update/$1'),'Update')." | ".anchor(site_url('basis_kasus/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_first_row()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->first_row();
    }

    function get_gejala($kode_kasus)
    {
        $this->db->where('kode_kasus', $kode_kasus);
        return $this->db->get('v_kasus_detail')->result();
    }

    function get_bobot($kode_kasus,$kode_gejala)
    {
        $this->db->where('kode_kasus', $kode_kasus);
        $this->db->where('kode_gejala', $kode_gejala);
        return $this->db->get('v_kasus_detail')->row();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get('v_basis_kasus')->row();
    }

    function get_by_kode($kode)
    {
        $this->db->where('kode', $kode);
        return $this->db->get('v_basis_kasus')->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
		$this->db->or_like('id_user', $q);
		$this->db->or_like('kode', $q);
		$this->db->or_like('pengobatan', $q);
		$this->db->or_like('penyebab', $q);
		$this->db->or_like('keterangan', $q);
		$this->db->or_like('kode_penyakit', $q);
		$this->db->or_like('created_at', $q);
		$this->db->or_like('updated_at', $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
		$this->db->or_like('id_user', $q);
		$this->db->or_like('kode', $q);
		$this->db->or_like('pengobatan', $q);
		$this->db->or_like('penyebab', $q);
		$this->db->or_like('keterangan', $q);
		$this->db->or_like('kode_penyakit', $q);
		$this->db->or_like('created_at', $q);
		$this->db->or_like('updated_at', $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* Location: ./application/models/Basis_kasus_model.php */
/* Please DO NOT modify this information : */
/* Kode ini di buat oleh Andre Bhaskoro pada tgl 2017-06-19 05:16:26 */
/* http://amertaproject.com | +6282333817317 */