<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gejala_model extends CI_Model
{
    public $table = 'gejala';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id,kode,gejala,bobot');
        $this->datatables->from('gejala');
        //add this line for join
        //$this->datatables->join('table2', 'gejala.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('gejala/read/$1'),'Read')." | ".anchor(site_url('gejala/update/$1'),'Update')." | ".anchor(site_url('gejala/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    
    function get_all_asc()
    {
        $this->db->order_by($this->id, 'ASC');
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_by_kode($kode)
    {
        $this->db->where('kode', $kode);
        return $this->db->get($this->table)->row();
    }

    function get_first_row()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->first_row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
		$this->db->or_like('kode', $q);
		$this->db->or_like('gejala', $q);
		$this->db->or_like('bobot', $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
		$this->db->or_like('kode', $q);
		$this->db->or_like('gejala', $q);
		$this->db->or_like('bobot', $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* Location: ./application/models/Gejala_model.php */
/* Please DO NOT modify this information : */
/* Kode ini di buat oleh Andre Bhaskoro pada tgl 2017-06-18 09:35:49 */
/* http://amertaproject.com | +6282333817317 */