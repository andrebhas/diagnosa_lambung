-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 04 Okt 2017 pada 08.38
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_lambung`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `basis_kasus`
--

CREATE TABLE `basis_kasus` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `pengobatan` text NOT NULL,
  `penyebab` text NOT NULL,
  `keterangan` text NOT NULL,
  `kode_penyakit` varchar(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `basis_kasus`
--

INSERT INTO `basis_kasus` (`id`, `id_user`, `kode`, `pengobatan`, `penyebab`, `keterangan`, `kode_penyakit`, `created_at`, `updated_at`) VALUES
(3, 1, 'K1', '<p>Harus diketahui dosis, cara pemberian, cara kerja, dan efek samping lanjutkan obat untuk waktu yang ditentukan, walaupun ketika gejala tidak ada usahakan agar setiap saat mudah mendapatkan antasida Antisifasi peningkatan kebutuhan akan antasida dalam periode-periode teretentu. Hindari pengobatan sendiri dengan antasida sistemik (bicarbonat soda) yang merubah keseimbangan asam dan basa, Hindarai obat-obatan ulcarogenik seperti salisilat, ibuprofen, kortikosteroid Gunakan asetaminofen (Tylenon) aspirin yang dinetralkan (jika toleransi) untuk menghilangkan nyeri. </p><ul><li>Gastritis yang tidak parah dapat diberikan obat antasida dan istirahat yang banyak  </li><li>makanan yang sudah dihaluskan seperti bubur, agar-agar sup krim. </li><li>Hindari makan-makanan yang berbumbu banyak dan merangsang seperti lombok, merica dan sejenis asam-asaman atau makanan yang mengandung minyak banyak. </li><li>Banyak minum seperti air teh, air jahe dengan soda ataupun cairan yang banyak mengandung karbonat.  </li><li>Usahakan makan teratur, sebelum makan 30 menit diharapkan minum obat yang sudah  diberikan oleh dokter misalnya antisida.</li></ul>', '<ul><li>Stress</li><li>Mengkonsumsi alkohol</li><li>Merokok</li><li>Obat-obatan (terutama obat-obat analgetik-anti inflamasi seperti: aspirin (antalgin, postan dll), salicylat, indometahacin, sulfonamide, steroid)  </li><li>Infeksi,bakteri atau virus yang mengeluarkan endotoksin  </li><li>Sekresi cairan pankreas atau empedu yang mengalir kembalim kelambung </li><li>Radiasi</li><li>Bahan-bahan yang bersifat korosif (merusak)</li><li>Terlambat makan</li></ul>', '<p>Data kasus lama sesuai dengan data yang diperoleh melalui studi pustaka</p>', 'P1', '2017-06-20 01:34:38', '2017-07-24 21:52:07'),
(11, 1, 'K2', '<p>Bila tidak ditemukan penyebabnya, dokter akan mengobati gejala-gejalanya. Antasid atau penghambat H2 seperti cimetidine, ranitidine atau famotidine dapat dicoba untuk jangka waktu singkat. Bila orang tersebut terinifeksi Helicobacter pylori di lapisan lambungnya, maka biasanya diberikan bismuth subsalisilate dan antibiotik seperti amoxicillin atau metronidazole. </p>', '<ul><li> Menelan udara (aerofagi)  </li><li>Regurgitasi (alir balik, refluks) asam dari lambung  </li><li>Iritasi lambung (gastritis)</li><li>Ulkus  gastrikum atau ulkus duodenalis  </li><li>Kanker lambung  </li><li>Peradangan kandung empedu (kolesistitis)  </li><li>Intoleransi laktosa (ketidakmampuan mencerna susu dan produknya)  </li><li> Kelainan gerakan usus  </li><li>Kecemasan atau depresi  </li></ul>', '<p>Dispepsia adalah suatu kondisi medis yang ditandai dengan nyeri atau rasa tidak nyaman pada perut bagian atas atau dada yang biasanya timbul setelah makan. Penyakit refluks gastroesofageal adalah salah satu penyebab dispepsia yang paling umum. Penyebab-penyebab utama lainnya antara lain makan terlalu banyak, makan terlalu cepat dan mengabaikan proses pengunyahan dan pencernaan melalui kelenjar liur dari makanan yang tepat. Dispepsia terjadi ketika otot-otot dari organ saluran pencernaan atau sarafsaraf yang mengendalikan organ-organ tersebut tidak berfungsi dengan baik. Dispepsia adalah penyakit kronis yang biasanya berlangsung bertahun-tahun, bahkan bisa seumur hidup.</p>', 'P2', '2017-06-20 04:44:47', '2017-07-24 21:53:07'),
(12, 1, 'K3', '<ul><li>Bedah.  </li><li>Radio terapi.  </li><li>Kemoterapi. Terapi terarah.</li></ul>', '<ul><li>Konsumsi makanan yang diasinkan serta diasapi   </li><li>arang mengkonsumsi buah-buahan serta sayuran   </li><li>Riwayat medis keluarga dimana terdapat kanker lambung   </li><li>Infeksi yang disebabkan oleh Helicobacter pylori, sebuah bakteri yang tinggal di lapisan   lendir dalam lambung.   </li><li>Radang lambung kronis, yang mengacu pada radang lambung jangka panjang.  </li><li>Pernicious anemia, yaitu penurunan jumlah sel darah merah yang terjadi saat saluran   pencernaan tidak dapat menyerap vitamin B12 dengan baik.   </li><li>Merokok  </li></ul>', '<p>Kanker lambung atau dikenal sebagai gastric cancer merupakan kanker yang berawal pada bagian lambung.Secara global, kanker lambung merupakan penyebab kematian akibat kanker urutan ke-2 bagi pria maupun wanita. Kanker ini umum ditemukan di wiliayah Asia Timur. Di Singapura sendiri, kanker ini merupakan urutan ke-6 penyebab kematian pada pria, dengan kemungkinan 1 kali dalam 50 kali rentang hidup terkena kanker lambung. Sementara, untuk wanita di Singapura, kanker ini merupakan kanker paling umum urutan ke-8. Setiap tahunnya, kanker lambung menjadi penyebab kematian untuk kira-kira 300 orang Singapura.</p>', 'P3', '2017-06-20 04:50:35', '2017-07-24 21:53:54'),
(13, 1, 'K4', '<p>Pengobatan penyakit asam lambung memiliki tahapan. Cara awal yang paling mudah untuk dilakukan adalah dengan mengganti menu makanan, yaitu beralih ke makanan-makanan yang rendah lemak, tidak terlalu asin, maupun terlalu pedas. Tapi ketika perubahan menu makanan tidak berhasil, obat-obatan akan digunakan untuk meredakan gejala yang dirasakan. Bagi penderita yang mengalami penyakit asam lambung secara kambuhan, dokter kemungkinan akan meresepkan obat untuk jangka panjang.</p><p>Jika langkah-langkah pengobatan di atas masih belum berhasil mengatasi GERD, prosedur operasi kemungkinan akan dipertimbangkan dan disarankan oleh dokter.</p>', '<p>Makanan dan obat-obatan yang harus dihindari, seperti  merokok. Kopi, alkohol, minuman yang  mengandung asam seperti jus jeruk, minuman cola, dan saus salad yang berbahan dasar cuka, dan bahan-bahan lain yang secara kuat merangsang perut untuk menghasilkan asam atau yang menghambat pengosongan perut harus dihindari.</p>', '<p>GERD adalah proses aliran balik/refluks yang berulang, dengan atau tanpa keluhan mukosa namun menimbulkan gangguan dari kualitas hidup manusia. Pada GERD, asam perut dan enzim mengalir kembali dari perut menuju kerongkongan, menyebabkan peradangan dan nyeri pada kerongkongan. Penyakit GERD ini merupakan fenomena biasa yang dapat timbul pada setiap orang sewaktuwaktu. Pada orang normal, refluks ini terjadi pada posisi tegak sewaktu habis makan. Karena sikap posisi tegak tadi dibantu oleh adanya kontraksi peristaltik primer, isi lambung yang mengalir masuk ke esofagus segera dikembalikan ke lambung. Keluhan tipikal dari GERD adalah nyeri dibelakang tulang dada (heart burn) menjalar ke tenggorokan, regurgitasi atau rasa asam di lidah, dan keluhan tipikal rasa nyeri dada.</p>', 'P4', '2017-06-20 04:54:00', '2017-10-04 01:34:32'),
(14, 2, 'K5', '<p>Penanggulangan  utama  diare disusun oleh Depkes RI dan Ikatan Dokter Anak Indonesia (IDAI) melalui Lima Langkah Tuntaskan Diare (Lintas Diare). Langkah-langkah tersebut yaitu:  </p><ul><li>Oralit formula baru  </li><li>Pemberian zinc selama 10 hari</li><li>Melanjutkan pemberian ASI dan makanan 4. pemberian antibiotika tertentu sesuai indikasi,  </li><li>konseling/nasihati ibu.</li></ul><p>Pertolongan pertama yang bisa dilakukan jika terserang gastroenteritis antara lain hindari kontak dengan terduga penyebab, pencegahan kekurangan cairan atau jangan sampai dehidrasi, dan istirahat yang cukup,serta selalu menjaga kebersihan diri dan lingkungan.</p>', '<p>Gastroenteritis bisa disebabkan karena infeksi dan noninfeksi.Penyebab GE terbesar adalah karena infeksi. Gastroenteritis infeksi bisa disebabkan oleh organisme virus, bakteri, dan atau  parasit. Tersering disebabkan oleh virus, yaitu rotavirus, yang terkait dengan diare akut. Sedangkan penyebab non-infeksi bisa terjadi karena alergi makanan, minuman, obat-obatan, dan keracunan, misalnya pada bayi menyusui karena ibunya mengalami perubahan pola diet.Efek samping makanan, minuman, dan obat yang dikonsumsi juta turut punya andil sebagai penyebab keluhan di perut ini.  </p>', '<p>Penyakit ini sering disebut diare atau mencret. Padahal mencret hanyalah salah satu dari kumpulan gejala gastroenteritis. Jika dilihat dari golongan umur dan frekuensinya, belum tentu juga semua mencret bisa disebut diare. Yang dimaksud diare menurut organisasi kesehatan dunia (World Health Organization/WHO) adalah kejadian buang air besar dengan bentuk tinja yang lebih cair dari biasanya, dengan frekuensi lebih sering dari biasanya, selama satu hari atau lebih. Jadi, konsistensi tinja atau kotoran yang ditekankan. Penyebutan diare pada bayi menyusui akan berbeda dengan dewasa. Bayi yang memperoleh air susu ibu (ASI) eksklusif biasanya mengeluarkan tinja yang agak cair, di mana frekuensinya bisa 5 kali sehari. Hal ini juga belum bisa disebut diare.</p>', 'P5', '2017-06-20 05:01:28', '2017-07-24 21:56:21'),
(15, 2, 'K6', '<p>Langkah pengobatan gastroparesis diawali dengan mengidentifikasi dan mengatasi penyebab utamanya. Setelah itu, tindakan yang bisa dilakukan adalah:</p><ul><li><strong>Mengubah menu makanan.</strong> Pasien disarankan untuk mengonsumsi makanan yang lebih mudah dicerna.</li><li><strong>Penyuntikan</strong> <strong><em>Botulinum toxin</em></strong> pada katup yang menghubungkan lambung dengan usus kecil <em>(pylor</em><em>ic sphincter</em><em>)</em>, untuk melemahkan otot katup sehingga dapat membuka lebih lama untuk pengosongan lambung.</li><li><strong>Obat-obatan</strong><strong>.</strong> Ada dua jenis obat yang digunakan pada gastroparesis, yaitu obat-obatan untuk merangsang otot lambung dan obat-obatan untuk menekan mual dan muntah.</li><li><strong>Stimulasi listrik</strong><strong>.</strong> Mengalirkan listrik melalui elektroda yang ditempelkan ke dinding lambung, dengan tujuan merangsang gerakan otot lambung. Namun manfaat dari terapi ini masih perlu diteliti lebih jauh.</li><li><strong>Pembedahan.</strong> Pada kasus yang parah di mana pasien sampai tidak bisa makan atau minum, maka akan dipasang selang ke usus kecil untuk memasukkan makanan dari luar. Dokter juga bisa memasang selang ventilasi lambung untuk mengurangi tekanan dari isi lambung.</li></ul>', '<p>Sampai saat ini, penyebab terjadinya gastroparesis belum diketahui secara pasti. Namun pada banyak kasus, gastroparesis diduga terjadi akibat rusaknya saraf yang mengontrol otot-otot lambung (saraf vagus).</p><p>Saraf vagus berfungsi mengatur semua proses pada saluran pencernaan manusia, termasuk mengirim sinyal pada otot lambung untuk berkontraksi mendorong makanan ke dalam usus kecil.</p><p>Beberapa kondisi lain yang juga dapat menyebabkan gastroparesis adalah:</p><ul><li><strong>Komplikasi dari beberapa jenis pembedahan.</strong> Seperti pembedahan untuk mengurangi berat badan (seperti <em>gastric banding</em>, atau <em>gastric bypass</em>) atau pengangkatan sebagian lambung (<em>gastrectomy</em>).</li><li><strong>Amyloidosis.</strong> Penyakit langka di mana terjadi penimbunan protein abnormal di dalam jaringan atau organ di seluruh tubuh.</li><li><strong>Obat-obatan</strong><strong>,</strong> seperti pereda nyeri golongan opioid dan beberapa antidepresan.</li><li><strong><a target="_blank" href="http://www.alodokter.com/scleroderma">Skleroderma</a>,</strong> penyakit yang menyebabkan pengerasan di beberapa bagian kulit dan kadang-kadang juga terdapat gangguan pada organ dalam dan pembuluh darah.</li><li><strong>Penyakit </strong><strong>P</strong><strong>arkinson,</strong> di mana terjadi kerusakan otak secara progresif.</li><li><strong>Diabetes tipe 1 atau </strong><strong>tipe </strong><strong>2 yang tidak terkontrol.</strong></li></ul><p>Faktor-faktor lain yang juga dapat menghambat proses pengosongan lambung yaitu:</p><ul><li>Infeksi, biasanya virus.</li><li>Hipotiroid.</li><li>Pembedahan perut atau esofagus.</li><li>Kelainan sistem saraf.</li><li><a target="_blank" href="http://www.alodokter.com/diabetes/">Diabetes</a>.</li><li>Skleroderma.</li><li>Obat-obatan yang menurunkan kecepatan pengosongan lambung.</li><li>Terapi radiasi.</li></ul>', '<p>Gastroparesis adalah kondisi dimana pergerakan spontan (motilitas) otot lambung tidak berjalan dengan semestinya.</p><p>Dalam keadaan normal, makanan didorong oleh kontraksi otot sepanjang saluran pencernaan. Pada penderita gastroparesis, motilitas lambung tidak bekerja dengan sempurna atau bahkan tidak bekerja sama sekali, sehingga menghambat proses pengosongan lambung. Hal ini tentunya akan mengganggu proses pencernaan secara keseluruhan, yang kemudian mengakibatkan gangguan nutrisi, masalah gula darah, dan sebagainya.</p>', 'P6', '2017-06-20 05:04:58', '2017-10-04 01:32:10'),
(16, 2, 'K7', '<p>Langkah pengobatan untuk tiap pasien tentu tidak sama. Dokter menentukannya berdasarkan pada penyebab tukak lambung yang dialami pasien. Tujuan utama pengobatan tukak lambung adalah memusnahkan bakteri <em>H. pylori </em>dan mengurangi konsumsi obat-obat antiinflamasi nonsteroid (NSAIDs). Penanganan tukak lambung juga dibantu dengan pemberian obat-obatan.</p><p>Beberapa jenis obat yang umumnya akan digunakan untuk menangani tukak lambung meliputi:</p><ul><li><strong>Antibiotik</strong>. Tukak lambung yang disebabkan oleh bakteri<em> H. pylori</em> akan ditangani dengan kombinasi dari beberapa antibiotik. <em>Amoxicillin,</em>, <em>metronidazole</em> dan <em>clarithromycin</em> adalah contoh antibiotik yang biasanya diresepkan oleh dokter. Pasien akan diperiksa kembali untuk melihat keberadaan bakteri <em>H. pylori</em> empat minggu setelah pengobatan dengan kombinasi antibiotik ini selesai. Jika bakteri tersebut masih tersisa, maka dokter akan memberi antibiotik yang berbeda untuk menuntaskan pengobatan.</li><li><strong>Penghambat pompa proton</strong>. Jika Anda mengidap tukak lambung yang disebabkan oleh obat anti inflamasi non-steroid, dokter akan menyarankan penggunaan penghambat pompa proton. Obat ini akan mengurangi kadar asam lambung dengan menghalangi kinerja sel-sel yang memproduksi asam lambung. <em>Lansoprazoleomeprazole, </em>dan<em> pantoprazole </em>adalah jenis penghambat pompa proton yang sering digunakan. Pemakaian obat-obatan ini bisa memberi efek samping yang biasanya ringan, seperti pusing, diare, atau sembelit, dan nyeri perut. Kendati demikian, efek samping ini akan hilang begitu pengobatan dihentikan. Obat ini biasanya diberikan selama empat hingga delapan minggu.</li><li><strong>Obat </strong><strong>penghambat reseptor H2</strong>. Fungsi obat ini sama dengan penghambat pompa proton, yaitu menurunkan kadar asam lambung.  Obat ini dapat mengatasi tukak lambung dan mempercepat kesembuhan. Obat yang biasa diberikan adalah ranitidin.</li><li><strong>Antasida dan alginat</strong>. Antasida akan menetralisasi asam lambung untuk waktu singkat, sedangkan alginat akan melindungi dinding lambung. Karena itu, kedua obat ini diberikan untuk mengurangi rasa nyeri secara cepat sebelum obat-obatan lainnya mulai bekerja. Kendati demikian, umumnya antasida tidak digunakan untuk mengatasi tukak lambung. Untuk penggunaannya, antasida sebaiknya dikonsumsi setelah makan. Tetapi jika Anda menggunakan penghambat pompa proton atau ranitidine, sebaiknya menunggu 1-2 jam sebelum mengonsumsi antasida dan alginat, karena dapat menimbulkan efek samping seperti perut kembung, diare, dan muntah. Buah pisang juga dapat dikonsumsi sebagai alternatif jika Anda enggan menggunakan kedua obat ini.</li><li><strong>Obat yang melindungi dinding lambung dan usus halus. </strong>Obat jenis <em>cytoprotective</em> ini adalah sukralfat dan misoprostol.</li></ul><p>Selain pemberian obat-obatan, tindakan operasi juga terkadang diperlukan jika tukak lambung telah mengakibatkan adanya lubang pada dinding lambung atau jika terjadi perdarahan serius yang tidak bisa diatasi dengan tindakan lewat endoskopi.</p>', '<p>Dinding lambung biasanya dilapisi selaput (mukus) yang melindunginya dari asam lambung. Peningkatan kadar asam lambung atau penipisan selaput pelindung lambung berpotensi memicu munculnya tukak lambung.</p><p>Penyebab umum yang dapat menurunkan perlindungan dinding lambung terhadap asam lambung meliputi infeksi bakteri <em>H</em><em>elicobacter pylori</em> dan penggunaan obat anti inflamasi non-steroid. Ibuprofen, aspirin, atau <em>diclofenac</em> adalah beberapa contoh obat anti inflamasi non-steroid yang sering digunakan.  Selain itu, penyakit tumor pankreas (gastrinoma) dan pengobatan radiasi pada area lambung juga dapat menyebabkan timbulnya tukak lambung.</p><p>Infeksi akibat bakteri <em>H.</em><em> pylori</em> termasuk kondisi yang umum dan sering kali tidak disadari penderitanya. Sementara konsumsi obat anti inflamasi non-steroid yang sering atau berkepanjangan akan meningkatkan risiko tukak lambung, terutama bagi lansia.</p><p>Di samping bakteri dan obat, faktor gaya hidup juga meningkatkan risiko menderita tukak lambung, seperti :</p><ul><li>Mengonsumsi minuman beralkohol yang dapat menipiskan selaput pelindung dinding lambung.</li><li>Mengalami stres yang tidak segera diatasi.</li><li>Merokok yang meningkatkan risiko mengalami tukak lambung bagi orang yang terinfeksi bakteri <em>pylori</em>.</li></ul><p>Banyak yang menganggap makanan pedas atau kondisi stres sebagai penyebab tukak lambung. Anggapan ini tidak benar. Makanan dan stres tidak menyebabkan tukak lambung, tapi dapat memperparah gejalanya.</p>', '<p>Tukak lambung (ulkus peptikum, <em>peptic ulcer</em>) adalah luka yang muncul pada dinding lambung akibat terkikisnya lapisan dinding lambung. Luka ini juga berpotensi muncul pada dinding bagian pertama usus kecil (duodenum) serta kerongkongan (esofagus). Tukak lambung dapat menyebabkan rasa nyeri pada lambung atau bahkan perdarahan dalam kasus yang parah.</p><p>Penyakit ini dapat menyerang semua orang pada segala umur. Meski begitu, pria usia di atas 60 tahun memiliki risiko yang lebih tinggi untuk mengalaminya. Tukak lambung sendiri dapat diobati hingga tuntas jika penyebab utama dapat diketahui.</p><p>Tukak lambung (ulkus peptikum, <em>peptic ulcer</em>) adalah luka yang muncul pada dinding lambung akibat terkikisnya lapisan dinding lambung. Luka ini juga berpotensi muncul pada dinding bagian pertama usus kecil (duodenum) serta kerongkongan (esofagus). Tukak lambung dapat menyebabkan rasa nyeri pada lambung atau bahkan perdarahan dalam kasus yang parah.</p><p>Penyakit ini dapat menyerang semua orang pada segala umur. Meski begitu, pria usia di atas 60 tahun memiliki risiko yang lebih tinggi untuk mengalaminya. Tukak lambung sendiri dapat diobati hingga tuntas jika penyebab utama dapat diketahui.</p><p> </p>', 'P7', '2017-06-20 05:07:22', '2017-10-04 01:28:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gejala`
--

CREATE TABLE `gejala` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) NOT NULL,
  `gejala` text NOT NULL,
  `bobot` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gejala`
--

INSERT INTO `gejala` (`id`, `kode`, `gejala`, `bobot`) VALUES
(1, 'G1', 'Rasa nyeri dan rasa tidak nyaman pada perut', 0.8),
(2, 'G2', 'Rasa penuh didaerah lambung / daerah perut', 0.7),
(3, 'G3', 'Perut kembung', 0.6),
(4, 'G4', 'Rasa mual', 0.8),
(5, 'G5', 'Muntah', 0.7),
(6, 'G6', 'Rasa nyeri pada uluhati', 0.6),
(7, 'G7', 'Selera makan berkurang', 0.7),
(8, 'G8', 'Rasa nyeri pada perut bagian atas', 0.8),
(9, 'G9', 'Sering Sendawa', 0.5),
(10, 'G10', 'Sering mendengar suara usus yang keras (borborigmi)', 0.4),
(11, 'G11', 'Sembelit', 0.3),
(12, 'G12', 'Diare', 0.4),
(13, 'G13', 'Kehilangan berat badan secara mendadak', 0.2),
(14, 'G14', 'Demam atau meriang', 0.6),
(15, 'G15', 'Kejang perut', 0.3),
(16, 'G16', 'Keluarnya BAB warna hitam pekat', 0.2),
(17, 'G17', 'pendarahan (BAB darah atau Muntah darah)', 0.2),
(18, 'G18', 'Rasa nyeri terbakar dibelakang tulang dada (heartburn) yang menyebar ke leher', 0.2),
(19, 'G19', 'Rasa ada makanan atau minuman balik ke mulut', 0.3),
(20, 'G20', 'Mulut terasa asam dan pahit', 0.2),
(21, 'G21', 'Batuk menahun', 0.1),
(22, 'G22', 'Serak dan tenggorokan sakit', 0.2),
(23, 'G23', 'Mudah lelah (lesu)', 0.5),
(24, 'G24', 'Perasaan kenyang berlebihan walaupunhanya makan sedikit', 0.7),
(25, 'G25', 'Dehidrasi berat', 0.3),
(26, 'G26', 'Rasa nyeri pada tukak deodenum', 0.2),
(27, 'G27', 'Sakit pada tukak lambung', 0.6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Akses Administrator'),
(4, 'pakar', 'Akses Pakar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kasus_baru`
--

CREATE TABLE `kasus_baru` (
  `id` int(11) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `usia` int(3) NOT NULL,
  `latar_belakang` varchar(100) NOT NULL,
  `nilai_knn` double DEFAULT NULL,
  `kode_similaritas` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kasus_detail`
--

CREATE TABLE `kasus_detail` (
  `id` int(11) NOT NULL,
  `kode_kasus` varchar(50) NOT NULL,
  `kode_gejala` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kasus_detail`
--

INSERT INTO `kasus_detail` (`id`, `kode_kasus`, `kode_gejala`) VALUES
(8, 'K1', 'G7'),
(9, 'K1', 'G6'),
(10, 'K1', 'G5'),
(11, 'K1', 'G4'),
(12, 'K1', 'G3'),
(13, 'K1', 'G2'),
(14, 'K1', 'G1'),
(15, 'K2', 'G12'),
(16, 'K2', 'G11'),
(17, 'K2', 'G10'),
(18, 'K2', 'G9'),
(19, 'K2', 'G8'),
(20, 'K2', 'G7'),
(21, 'K2', 'G6'),
(22, 'K2', 'G4'),
(23, 'K2', 'G3'),
(24, 'K3', 'G17'),
(25, 'K3', 'G16'),
(26, 'K3', 'G15'),
(27, 'K3', 'G14'),
(28, 'K3', 'G13'),
(29, 'K3', 'G8'),
(30, 'K3', 'G7'),
(31, 'K3', 'G5'),
(32, 'K3', 'G3'),
(42, 'K5', 'G25'),
(43, 'K5', 'G23'),
(44, 'K5', 'G15'),
(45, 'K5', 'G14'),
(46, 'K5', 'G12'),
(47, 'K5', 'G5'),
(48, 'K5', 'G4'),
(49, 'K5', 'G3'),
(50, 'K5', 'G1'),
(123, 'K7', 'G27'),
(124, 'K7', 'G26'),
(125, 'K7', 'G25'),
(126, 'K7', 'G7'),
(127, 'K7', 'G6'),
(128, 'K7', 'G5'),
(129, 'K7', 'G4'),
(130, 'K6', 'G24'),
(131, 'K6', 'G15'),
(132, 'K6', 'G13'),
(133, 'K6', 'G7'),
(134, 'K6', 'G5'),
(135, 'K6', 'G4'),
(136, 'K6', 'G3'),
(137, 'K6', 'G1'),
(138, 'K4', 'G22'),
(139, 'K4', 'G21'),
(140, 'K4', 'G20'),
(141, 'K4', 'G19'),
(142, 'K4', 'G18'),
(143, 'K4', 'G15'),
(144, 'K4', 'G14'),
(145, 'K4', 'G10'),
(146, 'K4', 'G3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyakit`
--

CREATE TABLE `penyakit` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) NOT NULL,
  `penyakit` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penyakit`
--

INSERT INTO `penyakit` (`id`, `kode`, `penyakit`) VALUES
(2, 'P1', 'Grastitis (maag)'),
(3, 'P2', 'Dispepsia'),
(4, 'P3', 'Kanker Lambung (Gastric Cancer)'),
(5, 'P4', 'Gastroesophageal Reflux Disease (GERD)'),
(6, 'P5', 'Gastroenteritis'),
(7, 'P6', 'Gastroparesis'),
(8, 'P7', 'Tukak lambung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `alamat` varchar(256) NOT NULL,
  `user_img` varchar(100) NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nama`, `email`, `username`, `password`, `phone`, `alamat`, `user_img`, `last_login`, `created_on`, `ip_address`, `active`, `salt`) VALUES
(1, 'Administrator', 'admin@admin.com', 'admin', '$2y$08$KJ5DNCAs3v8qwbsdsean/uIb8iwtS5H01LBO.MoNUcgdMu2velxD2', '0823338173177', 'Jln Cempaka No 38 Jember', 'usr_img_37f20a7.png', 1507097606, 1268889823, '127.0.0.1', 1, ''),
(2, 'Dr. Pakar', 'pakar1@mail.com', 'pakar', '$2y$08$KJ5DNCAs3v8qwbsdsean/uIb8iwtS5H01LBO.MoNUcgdMu2velxD2', '082333817317', 'Jember Jawa Timur', 'usr_img_a4300b0.png', 1507097768, 1497841262, '::1', 1, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 4);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_basis_kasus`
--
CREATE TABLE `v_basis_kasus` (
`id` int(11)
,`kode` varchar(50)
,`id_user` int(11)
,`nama` varchar(50)
,`email` varchar(100)
,`pengobatan` text
,`penyebab` text
,`keterangan` text
,`kode_penyakit` varchar(5)
,`penyakit` varchar(50)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_kasus_detail`
--
CREATE TABLE `v_kasus_detail` (
`id` int(11)
,`kode_kasus` varchar(50)
,`kode_gejala` varchar(5)
,`gejala` text
,`bobot` double
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_basis_kasus`
--
DROP TABLE IF EXISTS `v_basis_kasus`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_basis_kasus`  AS  select `basis_kasus`.`id` AS `id`,`basis_kasus`.`kode` AS `kode`,`basis_kasus`.`id_user` AS `id_user`,`users`.`nama` AS `nama`,`users`.`email` AS `email`,`basis_kasus`.`pengobatan` AS `pengobatan`,`basis_kasus`.`penyebab` AS `penyebab`,`basis_kasus`.`keterangan` AS `keterangan`,`penyakit`.`kode` AS `kode_penyakit`,`penyakit`.`penyakit` AS `penyakit`,`basis_kasus`.`created_at` AS `created_at`,`basis_kasus`.`updated_at` AS `updated_at` from ((`basis_kasus` join `users` on((`users`.`id` = `basis_kasus`.`id_user`))) join `penyakit` on((`penyakit`.`kode` = `basis_kasus`.`kode_penyakit`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_kasus_detail`
--
DROP TABLE IF EXISTS `v_kasus_detail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_kasus_detail`  AS  select `kasus_detail`.`id` AS `id`,`kasus_detail`.`kode_kasus` AS `kode_kasus`,`kasus_detail`.`kode_gejala` AS `kode_gejala`,`gejala`.`gejala` AS `gejala`,`gejala`.`bobot` AS `bobot` from (`kasus_detail` join `gejala` on((`gejala`.`kode` = `kasus_detail`.`kode_gejala`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `basis_kasus`
--
ALTER TABLE `basis_kasus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `gejala`
--
ALTER TABLE `gejala`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kasus_baru`
--
ALTER TABLE `kasus_baru`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `kasus_detail`
--
ALTER TABLE `kasus_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_kasus` (`kode_kasus`);

--
-- Indexes for table `penyakit`
--
ALTER TABLE `penyakit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `basis_kasus`
--
ALTER TABLE `basis_kasus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `gejala`
--
ALTER TABLE `gejala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kasus_baru`
--
ALTER TABLE `kasus_baru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kasus_detail`
--
ALTER TABLE `kasus_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT for table `penyakit`
--
ALTER TABLE `penyakit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
